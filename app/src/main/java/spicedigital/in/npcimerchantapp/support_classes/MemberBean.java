package spicedigital.in.npcimerchantapp.support_classes;

/**
 * Created by ch-e00812 on 3/20/2016.
 */
public class MemberBean {

    public String personName;
    public String mobileNumber;
    public String amountStatus;
    public String refernce;
    public String userid;
    public String walletVpa;
    public String adminId;
    public String groupName;
    public String groupId;

    public MemberBean(String personName, String mobileNumber, String amountStatus, String refernce, String userid, String walletVpa, String adminId, String groupName, String groupId) {
        this.personName = personName;
        this.mobileNumber = mobileNumber;
        this.amountStatus = amountStatus;
        this.refernce = refernce;
        this.userid = userid;
        this.walletVpa = walletVpa;
        this.adminId = adminId;
        this.groupName = groupName;
        this.groupId = groupId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAmountStatus() {
        return amountStatus;
    }

    public void setAmountStatus(String amountStatus) {
        this.amountStatus = amountStatus;
    }

    public String getRefernce() {
        return refernce;
    }

    public void setRefernce(String refernce) {
        this.refernce = refernce;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getWalletVpa() {
        return walletVpa;
    }

    public void setWalletVpa(String walletVpa) {
        this.walletVpa = walletVpa;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
