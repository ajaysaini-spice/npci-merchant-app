package spicedigital.in.npcimerchantapp.support_classes;

/**
 * Created by CH-E01006 on 3/19/2016.
 */
public class Constants {

    public static final String CORE_URL = "http://103.15.179.63:9020/ICICIApi/";

    public static final String PARSE_INSTALLATION_ID = "parseInstallationID";
    public static final String MOBILE_APP_VERSION = "mobileAppVersion";
    public static final String DEVICE_ID = "deviceId";
    public static final String EMAIL_ID = "emailId";
    public static final String IMEI = "imei";
    public static final String MOBILE_OS = "mobileOs";
    public static final String APP_NAME = "appName";
    public static final String VERSION_CODE = "version";
    public static final String PACKAGE_NAME = "packageName";
    public static final String PHONE_NO = "msisdn";

    public final static String isShowOTP = "false";
    public final static String isShowCreateGroup = "false";
    public final static String mobileNumber = "mobileNumber";
    public final static String isMainActivityOpened = "isMainActivityShown";

    public static final String VOLLEY_ERROR = "-111";
    public static final String NO_CONNECTION = "404";
    public static String NOINTERNET_LABEL="No internet connection";
    public static String NOINTERNET_LOWERLABEL="Seems like there is a problem with your internet connection.";



    public static final String REGISTERMEMBER_SUFFIX= "register?";
    public static final String ACCOUNTLINKING_SUFFIX = "accountLinking?";
    public static final String CREATEGROUP_SUFFIX = "createGroup?";
    public static final String INVITEMEMBER_SUFFIX= "addMember?";
    public static final String JOINMEMBER_SUFFIX= "joinmember?";

    public static final String DASHBOARD_SUFFIX= "dashboard?";
    public static final String MEMBER_SUFFIX= "dashboardMember?";
    public static final String BALANCE_SUFFIX= "accountBalance?";


    public static final String COLLECTION_SUFFIX= "paymentRequest?";
    public static final String PAYMENTREQUEST_SUFFIX= "paymentRequest?";



    public static final String REGISTER = "1";
    public static final String ACCOUNTLINKING = "2";
    public static final String CREATEGROUP = "3";
    public static final String INVITEMEMBER = "4";
    public static final String JOINMEMBER = "5";

    public static final String DASHBOARD = "6";
    public static final String BALANCE = "7";
    public static final String MEMBER = "8";

    public static final String COLLECTION = "9";
    public static final String PAYMENTREQUEST = "10";

}
