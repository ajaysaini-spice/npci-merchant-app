package spicedigital.in.npcimerchantapp.support_classes;

/**
 * Created by ch-e00812 on 3/19/2016.
 */
public class NoticeBoardBean {

    public String eventName;
    public String dateTime;
    public String description;
    public String location;
    public String address1;
    public String address2;
    public String address3;
    public String postedOn;
    public double latitude;
    public double longitude;
}
