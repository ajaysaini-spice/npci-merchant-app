package spicedigital.in.npcimerchantapp.support_classes;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Pattern;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.application.AppControler;

/**
 * Created by CH-E01006 on 3/19/2016.
 */
public class CommonUtility {


    public static void showSimpleSnackBar(Context context, String message, CoordinatorLayout coordinatorLayout) {
        try {
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, message, Snackbar.LENGTH_LONG).setDuration(Snackbar.LENGTH_LONG);
            try {
                TextView tv = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
                tv.setTypeface(font);
            } catch (Exception e) {

            }
            snackbar.show();
        } catch (Exception e) {
            displayToast(context, message, 1);

        }
    }


    /**
     * Display toast message
     *
     * @param mContext
     * @param message
     */
    public static void displayToast(Context mContext, String message,
                                    int numberOfTimes) {
        int count = 0;
        while (count < numberOfTimes) {
            ++count;
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("NewApi")
    public static void setStatusBarColor(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = ((Activity) context).getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(context.getResources().getColor(
                        R.color.transparentcolor));
            }
        } catch (Exception e) {
        }
    }

    public static void showNointernetDialog(Context context){
        try {
            final Dialog mOverlayDialog = new Dialog(context);
            mOverlayDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mOverlayDialog.setCancelable(true);
            mOverlayDialog.setContentView(R.layout.nonetwork_layout);
            final TextView txtlabel = (TextView) mOverlayDialog
                    .findViewById(R.id.txtlabel);
            txtlabel.setText(Constants.NOINTERNET_LABEL);
            final TextView txt_upperlabel = (TextView) mOverlayDialog
                    .findViewById(R.id.txt_upperlabel);
            txt_upperlabel.setText(Constants.NOINTERNET_LOWERLABEL);

            final TextView txt_ok = (TextView) mOverlayDialog
                    .findViewById(R.id.txt_ok);

            txt_ok.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    try {
                        mOverlayDialog.dismiss();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                }
            });
            mOverlayDialog.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static String getImeiNumber(Context context) {

        String deviceIMEI = "";
        try {
            TelephonyManager tManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            deviceIMEI = tManager.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceIMEI;
    }

    /**
     * Get Version Code of Application
     *
     * @param context
     * @return Version Code
     */

    public static String getVersionCode(Context context) {
        PackageInfo pInfo = null;
        int version = 0;
        try {
            pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
        if (pInfo != null)
            version = pInfo.versionCode;
        return String.valueOf(version);
    }

    /**
     * Get Email Id Registered with device
     *
     * @param context
     * @return Email Id
     */

    public static String getEmailId(Context context) {
        String possibleEmail = "";
        try {
            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
            Account[] accounts = AccountManager.get(context).getAccountsByType(
                    "com.google");
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    possibleEmail = account.name;
                    break;
                }
            }
        } catch (Exception e) {
            // Log.i("Exception", "Exception:" + e);
        }
        return possibleEmail;
    }

    public static String getDeviceId(Context context) {
        String deviceId = "";
        try {
            deviceId = Settings.Secure.getString(AppControler.getAppContext()
                    .getContentResolver(), Settings.Secure.ANDROID_ID);

            Log.e("DEVICE ID", "" + deviceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceId;
    }



    /**
     * Getting mobile network code
     *
     * @param mContext
     * @return MNC
     */
    public static String getMNC(Context mContext) {
        String mnc = "";
        try {
            // retrieve a reference to an instance of TelephonyManager
            TelephonyManager telephonyManager = (TelephonyManager) mContext
                    .getSystemService(Context.TELEPHONY_SERVICE);
            String networkOperator = telephonyManager.getNetworkOperator();
            mnc = networkOperator.substring(3);
        } catch (Exception e) {

        }
        return mnc;
    }

    /**
     * Getting CELL ID
     *
     * @param mContext
     * @return CELL ID
     */
    public static String getCellID(Context mContext) {
        String cellID = "";
        try {
            // retrieve a reference to an instance of TelephonyManager
            TelephonyManager telephonyManager = (TelephonyManager) mContext
                    .getSystemService(Context.TELEPHONY_SERVICE);
            GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager
                    .getCellLocation();
            cellID = String.valueOf(cellLocation.getCid());
        } catch (Exception e) {

        }
        return cellID;
    }

    /**
     * Getting location area code.
     *
     * @param mContext
     * @return LAC
     */
    public static String getLAC(Context mContext) {
        String lac = "";
        try {
            // retrieve a reference to an instance of TelephonyManager
            TelephonyManager telephonyManager = (TelephonyManager) mContext
                    .getSystemService(Context.TELEPHONY_SERVICE);
            GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager
                    .getCellLocation();
            lac = String.valueOf(cellLocation.getLac());
        } catch (Exception e) {

        }
        return lac;
    }

    public static void showAlert_dialogue(String message,boolean cancelable,Context myContext)
    {

        AlertDialog.Builder alert = new AlertDialog.Builder(myContext);


        alert.setMessage(message);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id)
            {
                //android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        alert.setCancelable(cancelable);
        alert.show();



    }
}
