package spicedigital.in.npcimerchantapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.database.MyDataBase;

/**
 * Created by CH-E01062 on 18-03-2016.
 */
public class PaymentRequestiesListAdapter extends BaseAdapter {


    Context mContext;
    ArrayList<HashMap<String, String>> mList;

    public PaymentRequestiesListAdapter(Context context, ArrayList<HashMap<String, String>> list) {

        mList = list;
        mContext = context;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.payment_requesties_item, null);
            holder = new ViewHolder();
            holder.raiseBy = (TextView) convertView.findViewById(R.id.raise_by);
            holder.status = (TextView) convertView.findViewById(R.id.status);

            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.amount = (TextView) convertView.findViewById(R.id.amount);
            holder.eventNmae = (TextView) convertView.findViewById(R.id.event_name_payment);
            holder.statusBackground = (FrameLayout) convertView.findViewById(R.id.status_background);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> recordData = mList.get(position);

        holder.eventNmae.setText(recordData.get(MyDataBase.KEY_EVENT_NAME));
        holder.raiseBy.setText(recordData.get(MyDataBase.KEY_UDF3));
        holder.status.setText(recordData.get(MyDataBase.KEY_CURRENT_STATUS));
        holder.date.setText(recordData.get(MyDataBase.KEY_CURRENT_DATE));
        holder.amount.setText(mContext.getResources().getString(R.string.rupees) + " " + recordData.get(MyDataBase.KEY_AMOUNT));


        if (recordData.get(MyDataBase.KEY_CURRENT_STATUS).equalsIgnoreCase("PAID")) {


            holder.statusBackground.setBackgroundColor(mContext.getResources().getColor(R.color.paymentApproveColor));
        } else if (recordData.get(MyDataBase.KEY_CURRENT_STATUS).equalsIgnoreCase("PENDING")) {

            holder.statusBackground.setBackgroundColor(mContext.getResources().getColor(R.color.paymentPendingColor));


        } else if (recordData.get(MyDataBase.KEY_CURRENT_STATUS).equalsIgnoreCase("Rejected")) {

            holder.statusBackground.setBackgroundColor(mContext.getResources().getColor(R.color.paymentRejectColor));


        }

        holder.date.setText(recordData.get(MyDataBase.KEY_EVENT_TIMESTAMP));
        holder.amount.setText(mContext.getResources().getString(R.string.rupees)+" "+recordData.get(MyDataBase.KEY_AMOUNT));


        return convertView;
    }

    static class ViewHolder {
        private TextView raiseBy, status, date, amount, eventNmae;
        private FrameLayout statusBackground;

    }
}
