package spicedigital.in.npcimerchantapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.R;


/**
 * Created by CH-E01039 on 2/25/2016.
 */
public class OperatorRechargeAdapter extends BaseAdapter {

    Context mContext;
    String[] items;

    public OperatorRechargeAdapter(Context mContext, String[] items) {

        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderItem viewHolder;
        try {
            if (convertView == null) {

                // inflate the layout
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(R.layout.add_community_type, parent, false);

                // well set up the ViewHolder
                viewHolder = new ViewHolderItem();


                viewHolder.item_text = (TextView) convertView.findViewById(R.id.item_text);

                // store the holder with the view.
                convertView.setTag(viewHolder);

            } else {
                // we've just avoided calling findViewById() on resource everytime
                // just use the viewHolder
                viewHolder = (ViewHolderItem) convertView.getTag();
            }
            try {

                viewHolder.item_text.setText(items[position]);


            } catch (Exception e) {
            }

        } catch (Exception e) {
        }

        return convertView;
    }

    static class ViewHolderItem {
        TextView item_text;
    }
}
