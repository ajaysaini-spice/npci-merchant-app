package spicedigital.in.npcimerchantapp.home_screen;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.MemberListAdapter;
import spicedigital.in.npcimerchantapp.application.AppControler;
import spicedigital.in.npcimerchantapp.database.MyDataBase;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.support_classes.MemberBean;
import spicedigital.in.npcimerchantapp.utils.CheckInternetConnection;
import spicedigital.in.npcimerchantapp.utils.CustomDialogNetworkRequest;
import spicedigital.in.npcimerchantapp.utils.VolleyResponse;

public class MembersList extends AppCompatActivity implements View.OnClickListener, VolleyResponse {

    ListView mListView;
    MemberListAdapter adapter;
    ArrayList<MemberBean> mDataList;
    Toolbar toolbar;
    FrameLayout allTab, refrences_btn;
    View view;
    TextView titleText;
    ImageView fabButton;
    private CustomDialogNetworkRequest requestClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members_list);
        //setData();
        mDataList = new ArrayList<>();
        initialiseToolbar();
        initialiseviews();

        getMembers(this);
    }


    public void initialiseviews() {
        try {
            allTab = (FrameLayout) findViewById(R.id.all_tab);
            allTab.setOnClickListener(this);
            refrences_btn = (FrameLayout) findViewById(R.id.refrences_btn);
            refrences_btn.setOnClickListener(this);

            fabButton = (ImageView) findViewById(R.id.fab_btn);
            fabButton.setOnClickListener(this);
            titleText = (TextView) view.findViewById(R.id.title_text);
            titleText.setText("MEMBERS");

            mListView = (ListView) findViewById(R.id.members_list);
            adapter = new MemberListAdapter(this, mDataList);
            mListView.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();

    }

    @Override
    public void onClick(View v) {
        try {
            if (v == allTab) {

                allTab.setBackgroundResource(R.drawable.tab_button_selected);
                refrences_btn.setBackgroundResource(R.drawable.tab_button_un_selected);

                adapter = new MemberListAdapter(MembersList.this, mDataList);
                mListView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            } else if (v == refrences_btn) {

                allTab.setBackgroundResource(R.drawable.tab_button_un_selected);

                refrences_btn.setBackgroundResource(R.drawable.tab_button_selected);


                ArrayList<MemberBean> tempAlist = new ArrayList<>();
                for (int i = 0; i < mDataList.size(); i++) {
                    if (mDataList.get(i).refernce.equalsIgnoreCase("true")) {
                        tempAlist.add(mDataList.get(i));
                    }
                }
                adapter = new MemberListAdapter(MembersList.this, tempAlist);
                mListView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }
            if (v == fabButton) {
                showDialogAddpassengers();


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* public void setData() {
        mDataList = new ArrayList<>();

        MemberBean bean = new MemberBean();
        bean.personName = "Mr. Gaurav Sidana";
        bean.mobileNumber = "+91-9876543210";
        bean.refernce = "false";

        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Rakesh Chander";
        bean.mobileNumber = "+91-9876543210";
        bean.refernce = "true";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Ram Lal";
        bean.mobileNumber = "+91-9876543210";
        bean.refernce = "false";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Salman";
        bean.mobileNumber = "+91-9876543210";
        bean.refernce = "true";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Rahul Sharma";
        bean.mobileNumber = "+91-9876543210";
        bean.refernce = "false";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Sandeep";
        bean.mobileNumber = "+91-9876543210";
        bean.refernce = "true";
        mDataList.add(bean);

    }*/

    public void showDialogAddpassengers() {

        try {
            final Dialog d = new Dialog(MembersList.this);
            d.requestWindowFeature(Window.FEATURE_NO_TITLE);
            d.setContentView(R.layout.dialog_addnewmember);
            TextView ca_mbnum = (TextView) d.findViewById(R.id.ca_mbnum);

            Button AddMember = (Button) d.findViewById(R.id.ca_done);
            AddMember.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    try {
                        d.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            Button ca_cancel = (Button) d.findViewById(R.id.ca_cancel);
            ca_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    try {
                        d.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            d.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getMembers(Context context) {
        try {

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);


            String url = Constants.CORE_URL + Constants.MEMBER_SUFFIX;

            HashMap<String, String> list = new HashMap<String, String>();

            list.put("groupId", sharedPreferences.getString("groupId", ""));
            list.put("userId", sharedPreferences.getString("userid", ""));

            list.put(Constants.IMEI,
                    CommonUtility.getImeiNumber(context));
            list.put(Constants.DEVICE_ID,
                    CommonUtility.getDeviceId(context));
            list.put(Constants.EMAIL_ID,
                    CommonUtility.getEmailId(context));
            list.put(Constants.MOBILE_OS, "Android"
                    + Build.VERSION.RELEASE);
            list.put("mobileApp", "true");
            list.put("mobileAppVersion", CommonUtility.getVersionCode(context));


            serverCommunication(url, list, Constants.MEMBER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void serverCommunication(String URL, HashMap<String, String> list, String resultCode) {


        if (CheckInternetConnection.haveNetworkConnection(this)) {
            try {
                requestClass = new CustomDialogNetworkRequest(MembersList.this, this);
                if (requestClass == null) {
                    requestClass = new CustomDialogNetworkRequest(MembersList.this, this);

                    requestClass.makePostRequest(URL, true, list, resultCode, "");
                } else {
                    requestClass.makePostRequest(URL, true, list, resultCode, "");
                }
            } catch (Exception e) {

            }


        } else {
            CommonUtility.showNointernetDialog(this);
        }
    }

    @Override
    public void onResult(String result, String responseCode) {
        if (responseCode.equalsIgnoreCase(Constants.MEMBER)) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(result);
                JSONObject responseObj = jsonObject.optJSONObject("response");
                String status = responseObj.optString("status");
                if (status.equalsIgnoreCase("S")) {
                    JSONArray jsonArray = responseObj.optJSONArray("dashboardUserList");
                    if(jsonArray!=null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String groupName = jsonObject1.optString("groupName");
                            String lastName = jsonObject1.optString("lastName");
                            String groupDesc = jsonObject1.optString("groupDesc");
                            String groupStatus = jsonObject1.optString("groupStatus");
                            String msisdn = jsonObject1.optString("msisdn");
                            String userid = jsonObject1.optString("userid");
                            String waaletName = jsonObject1.optString("waaletName");
                            String walletDesc = jsonObject1.optString("walletDesc");
                            String groupId = jsonObject1.optString("groupId");
                            String adminId = jsonObject1.optString("adminId");
                            String walletVpa = jsonObject1.optString("walletVpa");
                            String userStatus = jsonObject1.optString("userStatus");
                            String refCode = jsonObject1.optString("refCode");
                            String firstName = jsonObject1.optString("firstName");
                            String waaletId = jsonObject1.optString("waaletId");
                            String isadmin = jsonObject1.optString("isadmin");

                            MyDataBase dataBase = AppControler.getDataBaseInstance();
                            dataBase.insertMember(firstName + lastName, msisdn, userid, walletVpa, groupId, groupName, adminId, "", "", "", "", "", "", "");
                            MemberBean memberBean = new MemberBean(firstName + lastName, msisdn, userid, walletVpa, groupId, groupName, adminId, "", "");
                            mDataList.add(memberBean);
                        }
                        adapter = new MemberListAdapter(this, mDataList);
                        mListView.setAdapter(adapter);
                    }
                } else {

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
