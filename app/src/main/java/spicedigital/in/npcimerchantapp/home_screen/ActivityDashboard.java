package spicedigital.in.npcimerchantapp.home_screen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.R;

/**
 * Created by CH-E01039 on 3/19/2016.
 */
public class ActivityDashboard extends AppCompatActivity {


    Toolbar toolbar;
    View view;
    TextView titleText;

    TextView vpa, balance, account_number;

    Button editBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initialiseToolbar();
        initialiseviews();
    }


    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();
    }

    public void initialiseviews() {

        titleText = (TextView) view.findViewById(R.id.title_text);
        titleText.setText("DASHBOARD");

        vpa = (TextView) findViewById(R.id.vpa);
        balance = (TextView) findViewById(R.id.balance);
        account_number = (TextView) findViewById(R.id.account_number);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
