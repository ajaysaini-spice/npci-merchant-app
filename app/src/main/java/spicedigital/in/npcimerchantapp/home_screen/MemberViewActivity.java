package spicedigital.in.npcimerchantapp.home_screen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.ViewPaymentListMembers;
import spicedigital.in.npcimerchantapp.support_classes.MemberBean;

/**
 * Created by CH-E01062 on 19-03-2016.
 */
public class MemberViewActivity extends AppCompatActivity implements View.OnClickListener {




    ListView mListView;
    ViewPaymentListMembers adapter;
    ArrayList<MemberBean> mDataList;
    Toolbar toolbar;
    FrameLayout allTab,payeeTab,nonPayeeTab;
    View view;
    TextView titleText;

    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.members_detail_view);
        //setData();
        initialiseToolbar();
        initialiseviews();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void initialiseviews() {
      try {
          allTab = (FrameLayout) findViewById(R.id.all_tab);
          allTab.setOnClickListener(this);
          payeeTab = (FrameLayout) findViewById(R.id.payee_tab);
          payeeTab.setOnClickListener(this);

          nonPayeeTab = (FrameLayout) findViewById(R.id.non_payee_tab);

          nonPayeeTab.setOnClickListener(this);

          titleText = (TextView) view.findViewById(R.id.title_text);
          titleText.setText("MEMBERS DETAIL");

          mListView = (ListView) findViewById(R.id.payment_requisties_list);
          adapter = new ViewPaymentListMembers(this, mDataList);
          mListView.setAdapter(adapter);
      }catch(Exception e){
          e.printStackTrace();
      }
    }

    @Override
    public void onClick(View v) {
   try {
       if (v == allTab) {

           allTab.setBackgroundResource(R.drawable.tab_button_selected);
           nonPayeeTab.setBackgroundResource(R.drawable.tab_button_un_selected);

           payeeTab.setBackgroundResource(R.drawable.tab_button_un_selected);

           adapter = new ViewPaymentListMembers(MemberViewActivity.this, mDataList);
           mListView.setAdapter(adapter);
           adapter.notifyDataSetChanged();

       } else if (v == payeeTab) {


           allTab.setBackgroundResource(R.drawable.tab_button_un_selected);
           nonPayeeTab.setBackgroundResource(R.drawable.tab_button_un_selected);

           payeeTab.setBackgroundResource(R.drawable.tab_button_selected);


           ArrayList<MemberBean> tempAlist = new ArrayList<>();
           for (int i = 0; i < mDataList.size(); i++) {
               if (mDataList.get(i).amountStatus.equalsIgnoreCase("Paid")) {
                   tempAlist.add(mDataList.get(i));
               }
           }
           adapter = new ViewPaymentListMembers(MemberViewActivity.this, tempAlist);
           mListView.setAdapter(adapter);
           adapter.notifyDataSetChanged();

       } else if (v == nonPayeeTab) {


           allTab.setBackgroundResource(R.drawable.tab_button_un_selected);
           nonPayeeTab.setBackgroundResource(R.drawable.tab_button_selected);

           payeeTab.setBackgroundResource(R.drawable.tab_button_un_selected);

           ArrayList<MemberBean> tempAlist = new ArrayList<>();
           for (int i = 0; i < mDataList.size(); i++) {
               if (!mDataList.get(i).amountStatus.equalsIgnoreCase("Paid")) {
                   tempAlist.add(mDataList.get(i));
               }
           }
           adapter = new ViewPaymentListMembers(MemberViewActivity.this, tempAlist);
           mListView.setAdapter(adapter);
           adapter.notifyDataSetChanged();

       }
   }catch(Exception e){
       e.printStackTrace();
   }

    }


  /*  public void setData(){
        mDataList = new ArrayList<>();

        MemberBean bean = new MemberBean();
        bean.personName = "Mr. Gaurav Sidana";
        bean.mobileNumber = "+91-9876543210";
        bean.amountStatus = "Paid";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Rakesh Chander";
        bean.mobileNumber = "+91-9876543210";
        bean.amountStatus = "Not Paid";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Ram Lal";
        bean.mobileNumber = "+91-9876543210";
        bean.amountStatus = "Paid";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Salman";
        bean.mobileNumber = "+91-9876543210";
        bean.amountStatus = "Not Paid";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Rahul Sharma";
        bean.mobileNumber = "+91-9876543210";
        bean.amountStatus = "Paid";
        mDataList.add(bean);

        bean = new MemberBean();
        bean.personName = "Mr. Sandeep";
        bean.mobileNumber = "+91-9876543210";
        bean.amountStatus = "Not Paid";
        mDataList.add(bean);

    }

*/

}