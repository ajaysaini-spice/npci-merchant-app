package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseInstallation;

import org.json.JSONObject;

import java.util.HashMap;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.utils.CheckInternetConnection;
import spicedigital.in.npcimerchantapp.utils.CustomDialogNetworkRequest;
import spicedigital.in.npcimerchantapp.utils.VolleyResponse;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener,VolleyResponse {

    CustomDialogNetworkRequest requestClass;
    private EditText editTextMobileNumber;
    private Button buttonSubmit;
    private Context context;
    private SharedPreferences sharedPreferences;
    CoordinatorLayout coordinatorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secure_otp_layout);
        CommonUtility.setStatusBarColor(OtpActivity.this);
        context = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        /*if (sharedPreferences.getBoolean(Constants.isShowOTP, false)) {
            startActivity(new Intent(context, CreateJoinActivity.class));
            finish();
        }else  if(sharedPreferences.getBoolean(Constants.isShowCreateGroup, false)){
            startActivity(new Intent(context, CommunityDoneActivity.class));
            finish();
        }*/
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        editTextMobileNumber = (EditText) findViewById(R.id.editTextMobileNo);
        buttonSubmit = (Button) findViewById(R.id.btnSubmit);
        buttonSubmit.setOnClickListener(this);
    }

    public void addFieldsInInstallation() {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("mobileNumber", editTextMobileNumber.getText().toString());
        installation.saveInBackground();
    }

    @Override
    public void onClick(View v) {
        if (v == buttonSubmit) {

            if(validation()) {
                //addFieldsInInstallation();

                final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(OtpActivity.this);

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putBoolean(Constants.isShowOTP, true);
                edit.putString(Constants.mobileNumber, editTextMobileNumber.getText().toString().trim());
                edit.commit();

                registertation();
               // startActivity(new Intent(context, CreateJoinActivity.class));

            }
        }
    }

    public void registertation(){
        try{


            String url=Constants.CORE_URL+Constants.REGISTERMEMBER_SUFFIX;

            HashMap<String, String> list = new HashMap<String, String>();

            list.put("msisdn", editTextMobileNumber.getText().toString().trim());
            list.put("installationId", sharedPreferences.getString(Constants.PARSE_INSTALLATION_ID,""));

            list.put(Constants.IMEI,
                    CommonUtility.getImeiNumber(OtpActivity.this));
            list.put(Constants.DEVICE_ID,
                    CommonUtility.getDeviceId(OtpActivity.this));
            list.put(Constants.EMAIL_ID,
                    CommonUtility.getEmailId(OtpActivity.this));
            list.put(Constants.MOBILE_OS, "Android"
                    + Build.VERSION.RELEASE);
            list.put("mobileApp", "true");
            list.put("mobileAppVersion", CommonUtility.getVersionCode(OtpActivity.this));


            serverCommunication(url,list,Constants.REGISTER);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void serverCommunication(String URL, HashMap<String, String> list,String resultCode) {


        if (CheckInternetConnection.haveNetworkConnection(OtpActivity.this)) {
            try {
                requestClass = new CustomDialogNetworkRequest(OtpActivity.this, this);
                if (requestClass == null) {
                    requestClass = new CustomDialogNetworkRequest(OtpActivity.this, this);

                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                } else {
                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                }
            } catch (Exception e) {

            }


        } else {
            CommonUtility.showNointernetDialog(OtpActivity.this);
        }
    }

    @Override
    public void onResult(String result, String responseCode) {
        if (responseCode.equalsIgnoreCase(Constants.REGISTER)) {
            try{
                if(!result.equalsIgnoreCase("") && result != null) {
                    JSONObject responsejsonData = new JSONObject(result);
                    String response = responsejsonData.optString("response");

                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.optString("status");
                    if(status.equalsIgnoreCase("S")){

                        String groupDetailPojo = jsonData.optString("groupDetailPojo");
                        JSONObject userData = new JSONObject(groupDetailPojo);
                        String groupId = userData.optString("groupId");
                        String groupType = userData.optString("groupType");
                        String groupName = userData.optString("groupName");
                        String groupVpa = userData.optString("groupVpa");
                        String adminMsisdn = userData.optString("adminMsisdn");
                        String adminName = userData.optString("adminName");
                        String adminId = userData.optString("adminId");

                        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(OtpActivity.this);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("PersonType","MEMBER");
                        //editor.putString("userid",userid.trim());
                        editor.putString("groupId", groupId.trim());
                        editor.putString("name", adminName.toString());
                        editor.putString("communityVPA", groupVpa.toString());
                        editor.putString("adminMobile", adminMsisdn);
                        editor.putString("communityType", groupType.toString());
                        editor.putString("communityName", groupName.toString());
                        editor.commit();
                        startActivity(new Intent(context, CreateJoinActivity.class));

                    }else{
                        String errorCode = jsonData.optString("errorCode");
                        if(errorCode.equalsIgnoreCase("-2")){

                            String message= jsonData.optString("description");
                            CommonUtility.showAlert_dialogue(message,true,OtpActivity.this);

                        }else if(errorCode.equalsIgnoreCase("-1")){
                            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(OtpActivity.this);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("PersonType","ADMIN");
                            editor.commit();
                            startActivity(new Intent(context, CreateJoinActivity.class));

                        }

                    }



                }else{

                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public Boolean validation() {
        try {
            String mobileNo = editTextMobileNumber.getText().toString().trim();
            if (mobileNo.equalsIgnoreCase("")) {

                CommonUtility.showSimpleSnackBar(context, "Please enter mobile number", coordinatorLayout);
                editTextMobileNumber.requestFocus();
                return false;
            } else if (mobileNo.startsWith("0")) {

                CommonUtility.showSimpleSnackBar(context, "Mobile number cannot start with 0", coordinatorLayout);
                editTextMobileNumber.requestFocus();
                return false;
            } else if (mobileNo.length() > 10 || mobileNo.length() < 10) {

                CommonUtility.showSimpleSnackBar(context, "You have entered incorrect mobile number", coordinatorLayout);
                editTextMobileNumber.requestFocus();
                return false;
            }



    }catch(Exception e){
        e.printStackTrace();
    }
        return true;
    }
}
