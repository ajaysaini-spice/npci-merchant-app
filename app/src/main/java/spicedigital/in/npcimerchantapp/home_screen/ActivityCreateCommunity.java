package spicedigital.in.npcimerchantapp.home_screen;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Random;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.OperatorRechargeAdapter;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.utils.CheckInternetConnection;
import spicedigital.in.npcimerchantapp.utils.CustomDialogNetworkRequest;
import spicedigital.in.npcimerchantapp.utils.VolleyResponse;


/**
 * Created by CH-E01039 on 3/19/2016.
 */
public class ActivityCreateCommunity extends AppCompatActivity implements View.OnClickListener,VolleyResponse {

    CustomDialogNetworkRequest requestClass;
    EditText edittextName, editTextCommunityName, editTextDesc, editTextVPA,editTextAccount,editTextCustId;
    TextView textViewPowerBY;

    AutoCompleteTextView editTextCommunityType;
    Button btnSubmit;
    Context context;
    String[] choose_community;
    CheckBox checkSendpayRequest, checkViewtransactionSummary, checkViewAccountBalance, checkViewReferfriend;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_community);

        context = this;
        choose_community = context.getResources().getStringArray(R.array.community);
        initilizeView();
        setOnClickListener();

    }

    private void setOnClickListener() {
        btnSubmit.setOnClickListener(this);
        editTextCommunityType.setOnClickListener(this);
    }

    private void initilizeView() {
        try {
            edittextName = (EditText) findViewById(R.id.edittextName);
            editTextCommunityName = (EditText) findViewById(R.id.editTextCommunityName);
            editTextCommunityType = (AutoCompleteTextView) findViewById(R.id.editTextCommunityType);
            editTextDesc = (EditText) findViewById(R.id.editTextDesc);
            editTextVPA = (EditText) findViewById(R.id.editTextVPA);
            editTextAccount = (EditText) findViewById(R.id.editTextAccount);
            editTextCustId = (EditText) findViewById(R.id.editTextCustId);

            btnSubmit = (Button) findViewById(R.id.btnSubmit);
            textViewPowerBY = (TextView) findViewById(R.id.textViewPowerBY);
            checkSendpayRequest = (CheckBox) findViewById(R.id.checkSendpayRequest);
            checkViewtransactionSummary = (CheckBox) findViewById(R.id.checkViewtransactionSummary);
            checkViewAccountBalance = (CheckBox) findViewById(R.id.checkViewAccountBalance);
            checkViewReferfriend = (CheckBox) findViewById(R.id.checkViewReferfriend);
            coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

            editTextCommunityType.setClickable(true);
            editTextCommunityType.setFocusable(false);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                    (this, android.R.layout.select_dialog_item, getResources().getStringArray(R.array.community));


            editTextCommunityType.setThreshold(0);
            editTextCommunityType.setAdapter(adapter);
            editTextCommunityType.setFocusable(false);
            editTextCommunityType.setClickable(true);
            editTextCommunityType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editTextCommunityType.showDropDown();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void communityType() {
        ListView listView;
        TextView dialogtitle;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                final Dialog d = new Dialog(context, R.style.MaterialDialogSheet);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int screenHeight = (int) (metrics.heightPixels * 0.80);
//                d.setTitle("Select Recharge Type");
                //mBottomSheetDialog.setContentView (view);
                d.setCancelable(true);
                d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                        screenHeight);
                d.getWindow().setGravity(Gravity.BOTTOM);


                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.choose_community);
                // selectedPassengerdialog = new ArrayList<HashMap<String, String>>();


                listView = (ListView) d.findViewById(R.id.listView);
                dialogtitle = (TextView) d.findViewById(R.id.dialogtitle);
                dialogtitle.setText("Community Type");
                OperatorRechargeAdapter adapter = new OperatorRechargeAdapter(context, choose_community);
                listView.setAdapter(adapter);
                d.show();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        d.hide();
                        editTextCommunityType.setText(choose_community[position]);

                    }
                });
                d.show();


            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {

                try {
                    final Dialog d = new Dialog(context, R.style.PopupDialog);
                    // d.setCancelable(true);
                    //d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    int screenHeight = (int) (metrics.heightPixels * 0.80);
                    int screenWeight = (int) (metrics.widthPixels);
                    //mBottomSheetDialog.setContentView (view);
//                    d.setTitle("Select Recharge Type");
                    d.setCancelable(true);
                    d.getWindow().setLayout(screenWeight,
                            screenHeight);
                    d.getWindow().setGravity(Gravity.BOTTOM);
                    d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    d.setContentView(R.layout.choose_community);
                    // selectedPassengerdialog = new ArrayList<HashMap<String, String>>();


                    listView = (ListView) d.findViewById(R.id.listView);
                    dialogtitle = (TextView) d.findViewById(R.id.dialogtitle);
                    dialogtitle.setText("Community Type");
                    OperatorRechargeAdapter adapter = new OperatorRechargeAdapter(context, choose_community);
                    listView.setAdapter(adapter);
                    d.show();

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            d.hide();
                            editTextCommunityType.setText(choose_community[position]);

                        }
                    });
                    d.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnSubmit) {
            if (edittextName.getText().toString().length() == 0) {
                CommonUtility.showSimpleSnackBar(context, "Please enter your name", coordinatorLayout);
            } else if (editTextCommunityName.getText().toString().length() == 0) {
                CommonUtility.showSimpleSnackBar(context, "Please enter community name", coordinatorLayout);
            } else if (editTextCommunityType.getText().toString().length() == 0) {
                CommonUtility.showSimpleSnackBar(context, "Please enter community type", coordinatorLayout);
            } else if (editTextDesc.getText().toString().length() == 0) {
                CommonUtility.showSimpleSnackBar(context, "Please enter community description", coordinatorLayout);
            } else if (editTextVPA.getText().toString().length() == 0) {
                CommonUtility.showSimpleSnackBar(context, "Please enter community VPA", coordinatorLayout);
            } else {
                //saveDataToParse();
                createGroup();

            }
        } else if (v == editTextCommunityType) {
            communityType();
        }
    }


    private void saveDataToParse() {
        /*ParseQuery<ParseObject> queryUserAccount = new ParseQuery<ParseObject>(
                "Admin");
        queryUserAccount.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null) {*/

        ParseObject parseObject = new ParseObject("Admin");
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ActivityCreateCommunity.this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", edittextName.getText().toString());
        editor.putString("communityVPA", editTextVPA.getText().toString());

        editor.putString("PersonType", "ADMIN");
        editor.putString("communityType", editTextCommunityType.getText().toString());
        editor.putString("communityName", editTextCommunityName.getText().toString());



        editor.putString("description", editTextDesc.getText().toString());
        editor.putBoolean("viewAccountBal", checkViewAccountBalance.isChecked());
        editor.putBoolean("transactionSummary", checkViewtransactionSummary.isChecked());
        editor.putBoolean("referFriend", checkViewReferfriend.isChecked());
        editor.putBoolean("payRequest", checkSendpayRequest.isChecked());
        editor.commit();

        final String mobileNumber = sharedPreferences.getString(Constants.mobileNumber, "");

        ParseQuery<ParseObject> queryUserAccount = new ParseQuery<ParseObject>(
                "Admin");
        queryUserAccount.whereEqualTo("mobileNumber",
                mobileNumber);
        queryUserAccount.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putBoolean(Constants.isShowCreateGroup, true);
                edit.commit();

                if (e == null) {

                    // update values
                    parseObject.put("mobileNumber", mobileNumber);
                    parseObject.put("communityName", editTextCommunityName.getText().toString());
                    parseObject.put("name", edittextName.getText().toString());
                    parseObject.put("adminName", edittextName.getText().toString());
                    parseObject.put("communityType", editTextCommunityType.getText().toString());
                    parseObject.put("communityDesc", editTextDesc.getText().toString());
                    parseObject.put("communityVPA", editTextVPA.getText().toString());
                    parseObject.put("communityId", editTextCommunityName.getText().toString() + new Random().nextInt());
                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                        }
                    });
                    startActivity(new Intent(ActivityCreateCommunity.this, CommunityDoneActivity.class));



                } else {
                    ParseObject admin = new ParseObject("Admin");

                    if (mobileNumber != null && mobileNumber.length() > 0) {
                        admin.put("mobileNumber", mobileNumber);
                        admin.put("communityName", editTextCommunityName.getText().toString());
                        admin.put("name", edittextName.getText().toString());
                        admin.put("adminName", edittextName.getText().toString());
                        admin.put("communityType", editTextCommunityType.getText().toString());
                        admin.put("communityDesc", editTextDesc.getText().toString());
                        admin.put("communityVPA", editTextVPA.getText().toString());
                        admin.put("communityId", editTextCommunityName.getText().toString() + new Random().nextInt());
                        admin.saveInBackground();
                        startActivity(new Intent(ActivityCreateCommunity.this, CommunityDoneActivity.class));
                    }
                }
            }
        });


              /*  }
            }
        });*/
    }

public void createGroup(){
    try{
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ActivityCreateCommunity.this);
        final String mobileNumber = sharedPreferences.getString(Constants.mobileNumber, "");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", edittextName.getText().toString());
        editor.putString("communityVPA", editTextVPA.getText().toString());
        editor.putString("PersonType", "ADMIN");
        editor.putString("communityType", editTextCommunityType.getText().toString());
        editor.putString("communityName", editTextCommunityName.getText().toString());
        editor.putString("description", editTextDesc.getText().toString());
        editor.putBoolean("viewAccountBal", checkViewAccountBalance.isChecked());
        editor.putBoolean("transactionSummary", checkViewtransactionSummary.isChecked());
        editor.putBoolean("referFriend", checkViewReferfriend.isChecked());
        editor.putBoolean("payRequest", checkSendpayRequest.isChecked());
        editor.commit();


        String url=Constants.CORE_URL+Constants.CREATEGROUP_SUFFIX;

        HashMap<String, String> list = new HashMap<String, String>();

        list.put("firstName", edittextName.getText().toString());
        list.put("lastName", "");
        list.put("communityName", editTextCommunityName.getText().toString().trim());
        list.put("communityDesc", editTextDesc.getText().toString().trim());
        list.put("communityType",editTextCommunityType.getText().toString().trim());
        list.put("communityVpa", editTextVPA.getText().toString().trim());
        list.put("accountNo", editTextAccount.getText().toString().trim());
        list.put("custId", editTextCustId.getText().toString());

        list.put("msisdn", mobileNumber);
        list.put("type", "ADMIN");
        list.put("permission", "");
        list.put("installationId", sharedPreferences.getString(Constants.PARSE_INSTALLATION_ID,""));


        list.put(Constants.IMEI,
                CommonUtility.getImeiNumber(ActivityCreateCommunity.this));
        list.put(Constants.DEVICE_ID,
                CommonUtility.getDeviceId(ActivityCreateCommunity.this));
        list.put(Constants.EMAIL_ID,
                CommonUtility.getEmailId(ActivityCreateCommunity.this));
        list.put(Constants.MOBILE_OS, "Android"
                + Build.VERSION.RELEASE);
        list.put("mobileApp", "true");
        list.put("mobileAppVersion", CommonUtility.getVersionCode(ActivityCreateCommunity.this));


        serverCommunication(url,list,Constants.CREATEGROUP);
    }catch(Exception e){
        e.printStackTrace();
    }
}


    public void serverCommunication(String URL, HashMap<String, String> list,String resultCode) {


        if (CheckInternetConnection.haveNetworkConnection(ActivityCreateCommunity.this)) {
            try {
                requestClass = new CustomDialogNetworkRequest(ActivityCreateCommunity.this, this);
                if (requestClass == null) {
                    requestClass = new CustomDialogNetworkRequest(ActivityCreateCommunity.this, this);

                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                } else {
                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                }
            } catch (Exception e) {

            }


        } else {
            CommonUtility.showNointernetDialog(ActivityCreateCommunity.this);
        }
    }

    @Override
    public void onResult(String result, String responseCode) {
        if (responseCode.equalsIgnoreCase(Constants.CREATEGROUP)) {
            try{
                if(!result.equalsIgnoreCase("") && result != null) {
                    JSONObject responsejsonData = new JSONObject(result);
                    String response = responsejsonData.optString("response");

                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.optString("status");
                    if(status.equalsIgnoreCase("S")){
                        String message= jsonData.optString("description");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        String userInfo = jsonData.optString("userInfo");
                        JSONObject userData = new JSONObject(userInfo);
                        String userid = userData.optString("userid");
                        String groupId = userData.optString("groupId");

                        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ActivityCreateCommunity.this);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("userid",userid.trim());
                        editor.putString("groupId", groupId.trim());
                        editor.commit();
                        startActivity(new Intent(ActivityCreateCommunity.this, CommunityDoneActivity.class));
                    }else{

                        String message= jsonData.optString("description");
                        CommonUtility.showAlert_dialogue(message,true,ActivityCreateCommunity.this);
                    }



                }else{

                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
