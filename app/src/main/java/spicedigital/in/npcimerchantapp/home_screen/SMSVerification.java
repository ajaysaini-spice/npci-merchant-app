package spicedigital.in.npcimerchantapp.home_screen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.R;

public class SMSVerification extends AppCompatActivity {
    TextView textViewNumber,textViewlabel;
    EditText editTextotp;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsverification);
        intializeView();
    }

    public void intializeView(){
        try{

            textViewNumber = (TextView) findViewById(R.id.textViewNumber);
            textViewlabel = (TextView) findViewById(R.id.textViewlabel);

            editTextotp = (EditText) findViewById(R.id.editTextotp);
            btnSubmit = (Button) findViewById(R.id.btnSubmit);

        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
