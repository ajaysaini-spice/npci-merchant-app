package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.application.AppControler;
import spicedigital.in.npcimerchantapp.calender.CaldroidFragment;
import spicedigital.in.npcimerchantapp.calender.CaldroidListener;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.utils.CheckInternetConnection;
import spicedigital.in.npcimerchantapp.utils.CustomDialogNetworkRequest;
import spicedigital.in.npcimerchantapp.utils.VolleyResponse;
import spicedigital.in.npcimerchantapp.views.CollectionActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, VolleyResponse {


    LinearLayout layoutName, bannerView, paymentRequestView, collectionRequestView;
    FrameLayout walletView, memberView;
    LinearLayout offerView, expenses_graph;
    ImageView categoryImage;
    Toolbar toolbar;
    View view;
    TextView titleText, walletBalance, personName;
    ImageView menuIconToolbar;


    String PersonType = "";
    CustomDialogNetworkRequest requestClass;
    ImageView noticeBoard;
    SharedPreferences pref;

    TextView textViewPayment, textViewPayment2;
    TextView txtViewmoney, txtCompleted;
    TextView txtViewmoney2, txtCompleted2;
    SharedPreferences sharedPreferences;
    int i = 0;

    TextView monthlyBudgetTextView;


    Integer[] calendarArr = new Integer[]{
            R.drawable.calender,
            R.drawable.calender_21,
            R.drawable.calender_22,
            R.drawable.calender_23,
            R.drawable.calender_24,
            R.drawable.calender_25,
            R.drawable.calender_26,
            R.drawable.calender_27
    };
    private Context mContext;
    private PieChart mChart;
    private Typeface mTf;
    private ImageView offerImageView;
    private CaldroidFragment caldroidFragment;
    private LinearLayout calenderView;
    private CaldroidFragment dialogCaldroidFragment;
    private boolean undo = false;
    private LinearLayout calenderPlace;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        sharedPreferences = PreferenceManager

                .getDefaultSharedPreferences(getApplicationContext());

        PersonType = sharedPreferences.getString("PersonType", "");


        initializeViews();
        pieChart();
        setOnClickListener();
        animateoffers();
        displayCalender(savedInstanceState);

        if (!pref.getBoolean(Constants.isMainActivityOpened, false)) {
            insertData();
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean(Constants.isMainActivityOpened, true);
            edit.commit();
            dashboard();
        }


        getBalance();

    }

    private void displayCalender(Bundle savedInstanceState) {
        calenderPlace = (LinearLayout) findViewById(R.id.calendar1);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        int width = displaymetrics.widthPixels;


        calenderPlace.setLayoutParams(new LinearLayout.LayoutParams(width/2, LinearLayout.LayoutParams.WRAP_CONTENT));
        final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

        caldroidFragment = new CaldroidFragment();
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        }
        // If activity is created from fresh
        else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
            args.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS,true);


            // Uncomment this to customize startDayOfWeek
            // args.putInt(CaldroidFragment.START_DAY_OF_WEEK,
            // CaldroidFragment.TUESDAY); // Tuesday

            // Uncomment this line to use Caldroid in compact mode
            // args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);

            // Uncomment this line to use dark theme
           // args.putInt(CaldroidFragment.THEME_RESOURCE, R.style.CaldroidDefaultDark);

            caldroidFragment.setArguments(args);
        }

        setCustomResourceForDates();

        // Attach to the activity
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                Toast.makeText(getApplicationContext(), formatter.format(date),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChangeMonth(int month, int year) {
                String text = "month: " + month + " year: " + year;
                Toast.makeText(getApplicationContext(), text,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClickDate(Date date, View view) {
                Toast.makeText(getApplicationContext(),
                        "Long click " + formatter.format(date),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCaldroidViewCreated() {
                if (caldroidFragment.getLeftArrowButton() != null) {
                    Toast.makeText(getApplicationContext(),
                            "Caldroid view is created", Toast.LENGTH_SHORT)
                            .show();
                }
            }

        };

        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);


    }

    private void setCustomResourceForDates() {
        Calendar cal = Calendar.getInstance();

        // Min date is last 7 days
        cal.add(Calendar.DATE, -7);
        Date blueDate = cal.getTime();

        // Max date is next 7 days
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        Date greenDate = cal.getTime();

     /* if (caldroidFragment != null) {
            ColorDrawable blue = new ColorDrawable(getResources().getColor(R.color.blue));
            ColorDrawable green = new ColorDrawable(Color.GREEN);
            caldroidFragment.setBackgroundDrawableForDate(blue, blueDate);
            caldroidFragment.setBackgroundDrawableForDate(green, greenDate);
            caldroidFragment.setTextColorForDate(R.color.white, blueDate);
            caldroidFragment.setTextColorForDate(R.color.white, greenDate);
        }*/
    }

    private void animateoffers() {

       /* Drawable[] layers = new Drawable[3];
        layers[0] = mContext.getResources().getDrawable(R.drawable.offer);
        layers[1] = mContext.getResources().getDrawable(R.drawable.offer2);
        layers[2] = mContext.getResources().getDrawable(R.drawable.offe3);
        TransitionDrawable transition = new TransitionDrawable(layers);
        transition.setCrossFadeEnabled(true);

        offerImageView.setImageDrawable(transition);
        transition.startTransition(1000);
        transition.reverseTransition(1000);*/

        final int offers[] = {R.drawable.offer, R.drawable.offer2, R.drawable.offe3};

        try {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            offerImageView.setImageResource(offers[i]);
                            i++;
                            if (i == 3) {
                                i = 0;
                            }
                        }
                    });

                }
            }, 0, 4000);
        } catch (Exception e) {

        }

    }

    public void insertData() {
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

         /*   AppControler.getDataBaseInstance().addNotification("12345","5457","ICCTW20","Sports","","member","member","society name","society","20000","",
                    "19 March 2016","Pending","","","","","");

            AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12345", "5457", "Fuel Charges", "Bills", "", "society", "society", "merchant name", "merchant", "80000", "",
                    "18 March 2016", "Pending", "", "", "", "", "");*/


            if (PersonType.equalsIgnoreCase("ADMIN")) {

            /*
                AppControler.getDataBaseInstance().addAdminCollectionRequest("12345", "5457", "Monthly Charges", "Fund", "", "member", "member", "society name", "society", "80000", "",
                        "23 March 2016", "Pending", "", "", "", "", "");*/

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12346", "5458", "Property Tax", "Tax", "20 March 2016", "", "", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "30000", "",
                        "20 March 2016", "Pending", "500", "300", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12347", "5459", "Genset Replacement", "Maintenance", "22 March 2016", "", "", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "30000", "",
                        "22 March 2016", "Pending", "500", "200", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12348", "5460", "Membership Fees", "Funds", "23 March 2016", "", "", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "25000", "",
                        "23 March 2016", "Pending", "500", "100", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12349", "5461", "Water Bill", "Bills", "26 March 2016", "", "", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "15000", "",
                        "26 March 2016", "Pending", "500", "300", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12350", "5462", "Severage Charges", "Maintenance", "27 March 2016", "", "", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "40000", "",
                        "27 March 2016", "Pending", "500", "150", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12351", "5463", "New Year Party", "Party", "30 March 2016", "", "", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "30000", "",
                        "30 March 2016", "Pending", "500", "100", "", "", "");


                AppControler.getDataBaseInstance().addNotification("12346", "5458", "Property Tax", "Tax", "20 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "20 March 2016", "Paid", "500", "300", "Singla", "", "");

                AppControler.getDataBaseInstance().addNotification("12347", "5459", "Genset Replacement", "Maintenance", "22 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Modern Colony", "ModColony", "30000", "",
                        "22 March 2016", "Rejected", "500", "200", "Kings", "", "");

                AppControler.getDataBaseInstance().addNotification("12348", "5460", "Membership Fees", "Funds", "23 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Sunny Enclave", "SunEnclave", "25000", "",
                        "23 March 2016", "Paid", "500", "100", "Ankur", "", "");

                AppControler.getDataBaseInstance().addNotification("12349", "5461", "Water Bill", "Bills", "26 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Sun City", "SunCity", "15000", "",
                        "26 March 2016", "Pending", "500", "300", "Singh", "", "");

                AppControler.getDataBaseInstance().addNotification("12350", "5462", "Severage Charges", "Maintenance", "27 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Modern Colony", "ModColony", "40000", "",
                        "27 March 2016", "Rejected", "500", "150", "Nishu", "", "");

                AppControler.getDataBaseInstance().addNotification("12351", "5463", "New Year Party", "Party", "30 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "30 March 2016", "Paid", "500", "100", "R K Verma", "", "");
            } else {

                AppControler.getDataBaseInstance().addNotification("23346", "15458", "Property Tax", "Tax", "20 March 2016", sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "20 March 2016", "Pending", "500", "300", "Sachin", "", "");

                AppControler.getDataBaseInstance().addNotification("23347", "15459", "Genset Replacement", "Maintenance", "21 March 2016", sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Modern Colony", "ModColony", "30000", "",
                        "22 March 2016", "Pending", "500", "200", "Mark", "", "");

                AppControler.getDataBaseInstance().addNotification("23348", "15460", "Membership Fees", "Funds", "23 March 2016", sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Sunny Enclave", "SunEnclave", "25000", "",
                        "23 March 2016", "Pending", "500", "100", "J K", "", "");

                AppControler.getDataBaseInstance().addNotification("22349", "15461", "Water Bill", "Bills", "26 March 2016", sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Sun City", "SunCity", "15000", "",
                        "26 March 2016", "Pending", "500", "300", "V Sharama", "", "");

                AppControler.getDataBaseInstance().addNotification("22350", "15462", "Severage Charges", "Maintenance", "27 March 2016", sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Modern Colony", "ModColony", "40000", "",
                        "27 March 2016", "Pending", "500", "150", "Singla", "", "");

                AppControler.getDataBaseInstance().addNotification("22351", "5463", "New Year Party", "Party", "30", sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "30 March 2016", "Pending", "500", "100", "Ankur", "", "");


                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12346", "5458", "Property Tax", "Tax", "20 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "20 March 2016", "Paid", "500", "300", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12347", "5459", "Genset Replacement", "Maintenance", "21 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Modern Colony", "ModColony", "30000", "",
                        "22 March 2016", "Rejected", "500", "200", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12348", "5460", "Membership Fees", "Funds", "23 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Sunny Enclave", "SunEnclave", "25000", "",
                        "23 March 2016", "Paid", "500", "100", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12349", "5461", "Water Bill", "Bills", "26 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Sun City", "SunCity", "15000", "",
                        "26 March 2016", "Pending", "500", "300", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12350", "5462", "Severage Charges", "Maintenance", "27 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Modern Colony", "ModColony", "40000", "",
                        "27 March 2016", "Rejected", "500", "150", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12351", "5463", "New Year Party", "Party", "30 March 2016", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "30 March 2016", "Paid", "500", "100", sharedPreferences.getString("First name", ""), "", "");

            }

        } catch (Exception e) {

        }
    }

    public void setOnClickListener() {
        layoutName.setOnClickListener(this);
        walletView.setOnClickListener(this);
        memberView.setOnClickListener(this);
        paymentRequestView.setOnClickListener(this);
        collectionRequestView.setOnClickListener(this);
        offerView.setOnClickListener(this);
        bannerView.setOnClickListener(this);


    }

    public void initializeViews() {

        mChart = (PieChart) findViewById(R.id.pieChart);
        mTf = Typeface.createFromAsset(mContext.getAssets(),
                "fonts/Lato-Regular.ttf");

        categoryImage = (ImageView) findViewById(R.id.category_image);
        offerImageView = (ImageView) findViewById(R.id.offerImageView);
        monthlyBudgetTextView = (TextView) findViewById(R.id.monthlyBudgetTextView);


        pref = PreferenceManager.getDefaultSharedPreferences(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        view = toolbar.getRootView();
        titleText = (TextView) view.findViewById(R.id.title_text);
        titleText.setText("GREEN HOUSE SOCIETY");

        personName = (TextView) findViewById(R.id.person_name);


        textViewPayment = (TextView) findViewById(R.id.textViewPayment);
        textViewPayment2 = (TextView) findViewById(R.id.textViewPayment2);

        txtViewmoney = (TextView) findViewById(R.id.txtViewmoney);
        txtCompleted = (TextView) findViewById(R.id.txtCompleted);

        txtViewmoney2 = (TextView) findViewById(R.id.txtViewmoney2);
        txtCompleted2 = (TextView) findViewById(R.id.txtCompleted2);
        if (PersonType.equalsIgnoreCase("ADMIN")) {
            textViewPayment.setText("Payment Requests");
            textViewPayment2.setText("Collection Requests");
            txtViewmoney.setText("20");
            txtCompleted.setText("Completed");
            txtViewmoney2.setText("47");
            txtCompleted2.setText("Pending");
            personName.setText(pref.getString("name", ""));
        } else {
            textViewPayment.setText("Initiate Payments");
            textViewPayment2.setText("Payment Requests");
            txtViewmoney2.setText("20");
            txtCompleted2.setText("Completed");
            txtViewmoney.setText("47");
            txtCompleted.setText("Pending");
            personName.setText(pref.getString("membername", ""));
        }

        noticeBoard = (ImageView) view.findViewById(R.id.notice_board);
        noticeBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NoticeBoardScreen.class));
            }
        });
        titleText.setText(pref.getString("communityType", ""));

        walletBalance = (TextView) findViewById(R.id.wallet_balance);
        walletBalance.setText(getResources().getString(R.string.rupees) + "12,040");

        bannerView = (LinearLayout) findViewById(R.id.banner);
        menuIconToolbar = (ImageView) view.findViewById(R.id.menu_icon_toolbar);
        menuIconToolbar.setVisibility(View.VISIBLE);
        layoutName = (LinearLayout) findViewById(R.id.layout_name);
        walletView = (FrameLayout) findViewById(R.id.wallet_view);
        memberView = (FrameLayout) findViewById(R.id.member_view);
        paymentRequestView = (LinearLayout) findViewById(R.id.payment_request_view);
        collectionRequestView = (LinearLayout) findViewById(R.id.collection_request_view);
        offerView = (LinearLayout) findViewById(R.id.offer_view);
        expenses_graph = (LinearLayout) findViewById(R.id.expenses_graph);
        expenses_graph.setOnClickListener(this);
        setOnClickListener();
        setCategoryImage();
        setCalendarImage();
    }

    private void setCategoryImage() {

        if (pref.getString("communityType", "").equalsIgnoreCase("Housing Society")) {
            categoryImage.setImageResource(R.drawable.housing);
        } else if (pref.getString("communityType", "").equalsIgnoreCase("NGO")) {
            categoryImage.setImageResource(R.drawable.ngo);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("School")) {
            categoryImage.setImageResource(R.drawable.school);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("Self help group")) {
            categoryImage.setImageResource(R.drawable.self);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("Microsofinance industry")) {
            categoryImage.setImageResource(R.drawable.finance);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("Micro insurance provider")) {
            categoryImage.setImageResource(R.drawable.insurance);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("NBFC")) {
            categoryImage.setImageResource(R.drawable.nbfc);

        }

    }


    private void setCalendarImage() {

        DateFormat dateFormat = new SimpleDateFormat("dd");
        Date date = new Date();
        String todayDay = dateFormat.format(date);

        /*if (todayDay.equalsIgnoreCase("19")) {
            calenderView.setImageResource(calendarArr[0]);
        } else if (todayDay.equalsIgnoreCase("20")) {
            calenderView.setImageResource(calendarArr[1]);
        } else if (todayDay.equalsIgnoreCase("21")) {
            calenderView.setImageResource(calendarArr[1]);
        } else if (todayDay.equalsIgnoreCase("22")) {
            calenderView.setImageResource(calendarArr[2]);
        } else if (todayDay.equalsIgnoreCase("23")) {
            calenderView.setImageResource(calendarArr[3]);
        } else if (todayDay.equalsIgnoreCase("24")) {
            calenderView.setImageResource(calendarArr[4]);
        } else if (todayDay.equalsIgnoreCase("25")) {
            calenderView.setImageResource(calendarArr[5]);
        } else if (todayDay.equalsIgnoreCase("26")) {
            calenderView.setImageResource(calendarArr[6]);
        } else if (todayDay.equalsIgnoreCase("27")) {
            calenderView.setImageResource(calendarArr[7]);
        }*/


    }


    @Override
    public void onClick(View v) {

        if (v == collectionRequestView) {


//            new FragmentCreateCommunity().show(getSupportFragmentManager(), "TAG");

            if (PersonType.equalsIgnoreCase("ADMIN")) {
                startActivity(new Intent(this, CollectionActivity.class));
            } else {
                startActivity(new Intent(this, PaymentRequestforMembers.class));
            }
            //sendCollectRequest();

        } else if (v == bannerView) {
            startActivity(new Intent(this, NoticeBoardScreen.class));
        } else if (v == offerView) {

        } else if (v == layoutName) {
            startActivity(new Intent(this, MyProfileScreen.class));
        } else if (v == paymentRequestView) {

            startActivity(new Intent(this, PaymentRequestiesActivity.class));


            // sendRequesToPSPApp();


        } else if (v == memberView) {

            startActivity(new Intent(this, MembersList.class));


        } else if (v == calenderView) {
            startActivity(new Intent(this, CalendarScreen.class));
        } else if (v == expenses_graph) {
            startActivity(new Intent(this, ActivityDashboard.class));


        }


    }

    public void dashboard() {
        try {


            String url = Constants.CORE_URL + Constants.DASHBOARD_SUFFIX;

            HashMap<String, String> list = new HashMap<String, String>();

            list.put("groupId", sharedPreferences.getString("groupId", ""));

            list.put("userid", sharedPreferences.getString("userid", ""));
            list.put("installationId", sharedPreferences.getString(Constants.PARSE_INSTALLATION_ID, ""));

            list.put(Constants.IMEI,
                    CommonUtility.getImeiNumber(MainActivity.this));
            list.put(Constants.DEVICE_ID,
                    CommonUtility.getDeviceId(MainActivity.this));
            list.put(Constants.EMAIL_ID,
                    CommonUtility.getEmailId(MainActivity.this));
            list.put(Constants.MOBILE_OS, "Android"
                    + Build.VERSION.RELEASE);
            list.put("mobileApp", "true");
            list.put("mobileAppVersion", CommonUtility.getVersionCode(MainActivity.this));


            serverCommunication(url, list, Constants.DASHBOARD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void serverCommunication(String URL, HashMap<String, String> list, String resultCode) {


        if (CheckInternetConnection.haveNetworkConnection(MainActivity.this)) {
            try {
                requestClass = new CustomDialogNetworkRequest(MainActivity.this, this);
                if (requestClass == null) {
                    requestClass = new CustomDialogNetworkRequest(MainActivity.this, this);

                    requestClass.makePostRequest(URL, false, list, resultCode, "");
                } else {
                    requestClass.makePostRequest(URL, false, list, resultCode, "");
                }
            } catch (Exception e) {

            }


        } else {
            CommonUtility.showNointernetDialog(MainActivity.this);
        }
    }

    @Override
    public void onResult(String result, String responseCode) {
        if (responseCode.equalsIgnoreCase(Constants.DASHBOARD)) {
            try {
                if (!result.equalsIgnoreCase("") && result != null) {

                    JSONObject responsejsonData = new JSONObject(result);
                    String response = responsejsonData.optString("response");

                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.optString("status");

                    if (status.equalsIgnoreCase("S")) {
                        JSONArray jsonArray = jsonData.getJSONArray("dashboardCollectionReq");

                        JSONArray jsonArrayPayment = jsonData.getJSONArray("dashboardPaymentReq");

                        if (jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject payload_Data = jsonArray.getJSONObject(i);

                                String collectionId = payload_Data.optString("collectionId");
                                String collectionType = payload_Data.optString("collectionType");


                                String eventDate = payload_Data.optString("eventDate");
                                String[] date = null;
                                if (!eventDate.equalsIgnoreCase("")) {
                                    date = eventDate.split(" ");
                                }
                                String eventName = payload_Data.optString("eventName");
                                String eventDesc = payload_Data.optString("eventDesc");

                                String marchiantVpa = payload_Data.optString("marchiantVpa");
                                String marchiantName = payload_Data.optString("marchiantName");
                                String payerVpa = payload_Data.optString("payerVpa");
                                String payerName = payload_Data.optString("payerName");

                                String amount = payload_Data.optString("amount");
                                String groupId = payload_Data.optString("groupId");
                                String userId = payload_Data.optString("userId");
                                String msisdn = payload_Data.optString("msisdn");
                                String raised_by_name = payload_Data.optString("raised_by_name");


                                if (PersonType.equalsIgnoreCase("ADMIN")) {
                                    AppControler.getDataBaseInstance().addAdminCollectionRequest(collectionId, groupId, eventName, "", date[0], payerName, payerVpa, marchiantName, marchiantVpa, amount, eventDesc, "", "Pending", "100", "200", raised_by_name, "", "");
                                } else {
                                    AppControler.getDataBaseInstance().addNotification(collectionId, groupId, eventName, "", date[0], payerName, payerVpa, marchiantName, marchiantVpa, amount, eventDesc, "", "Pending", "100", "200", raised_by_name, "", "");

                                }

                            }
                        }

                        if (jsonArrayPayment.length() > 0) {
                            for (int i = 0; i < jsonArrayPayment.length(); i++) {

                                JSONObject payload_Data = jsonArrayPayment.getJSONObject(i);

                                String collectionId = payload_Data.optString("collectionId");
                                String collectionType = payload_Data.optString("collectionType");


                                String eventDate = payload_Data.optString("eventDate");
                                String[] date = null;
                                if (!eventDate.equalsIgnoreCase("")) {
                                    date = eventDate.split(" ");
                                }
                                String eventName = payload_Data.optString("eventName");
                                String eventDesc = payload_Data.optString("eventDesc");

                                String marchiantVpa = payload_Data.optString("marchiantVpa");
                                String marchiantName = payload_Data.optString("marchiantName");
                                String payerVpa = payload_Data.optString("payerVpa");
                                String payerName = payload_Data.optString("payerName");

                                String amount = payload_Data.optString("amount");
                                String groupId = payload_Data.optString("groupId");
                                String userId = payload_Data.optString("userId");
                                String msisdn = payload_Data.optString("msisdn");
                                String raised_by_name = payload_Data.optString("raised_by_name");

                                if (PersonType.equalsIgnoreCase("ADMIN")) {

                                    AppControler.getDataBaseInstance().addNotification(collectionId, groupId, eventName, "", date[0], payerName, payerVpa, marchiantName, marchiantVpa, amount, eventDesc, "", "Pending", "100", "200", raised_by_name, "", "");

                                } else {
                                    AppControler.getDataBaseInstance().addMemberApprovalPayRequest(collectionId, groupId, eventName, "", date[0], payerName, payerVpa, marchiantName, marchiantVpa, amount, eventDesc, "", "Pending", "100", "200", raised_by_name, "", "");

                                }

                            }
                        }

                    } else {


                    }
                } else {

                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } else if (responseCode.equalsIgnoreCase(Constants.BALANCE)) {
            if (!result.equalsIgnoreCase("") && result != null) {
                try {
                    JSONObject responsejsonData = new JSONObject(result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void pieChart() {
        mChart.setUsePercentValues(true);
        mChart.setDescription("");
        mChart.setExtraOffsets(5, 10, 0, 5);


        mChart.setDragDecelerationFrictionCoef(0.95f);


        mChart.setCenterTextTypeface(mTf);
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);

        String monthStr = c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);


        monthlyBudgetTextView.setText(getResources().getString(R.string.rupees) + "10000");

        mChart.setCenterText(generateCenterSpannableText(getResources().getString(R.string.rupees) + "10,130\n" + monthStr + "," + year));

        //   mChart.setCenterText("Rs 1047.0");

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.parseColor("#2C2C2E"));
        mChart.setTransparentCircleAlpha(255);

        mChart.setHoleRadius(80f);
        mChart.setTransparentCircleRadius(85f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        //  mChart.setOnChartValueSelectedListener(this);

        setData(3, new Random().nextFloat());

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);

        Legend l = mChart.getLegend();

        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
        // l.setStackSpace(10);
        l.setXEntrySpace(5f);
        l.setYEntrySpace(8f);
        l.setWordWrapEnabled(true);
        l.setTextColor(Color.WHITE);
        l.setYOffset(-10f);
        l.setXOffset(20f);
        l.setFormSize(12);
        //l.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
    }

    private SpannableString generateCenterSpannableText(String data) {

        SpannableString s = new SpannableString(data);

        s.setSpan(new RelativeSizeSpan(1.7f), 0, data.indexOf("\n"), 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length(), 0);
       /* s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);*/
        return s;
    }

    private void setData(int count, float range) {

        float mult = range;
        String[] mParties = new String[]{
                "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
                "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
                "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
                "Party Y", "Party Z"
        };

        String[] categories = new String[]{"Bills", "Events", "Security",
                "Fuel"};

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        for (int i = 0; i < count + 1; i++) {
            yVals1.add(new Entry((float) (Math.random() * mult) + mult / 2, i));
        }

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < count + 1; i++)
            xVals.add(categories[i]);

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        /*for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);*/

        colors.add(mContext.getResources().getColor(R.color.graphColor1));
        colors.add(mContext.getResources().getColor(R.color.graphColor2));
        colors.add(mContext.getResources().getColor(R.color.graphColor3));
        colors.add(mContext.getResources().getColor(R.color.graphColor4));


        // colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(0f);
        data.setValueTextColor(Color.WHITE);
        // data.setValueTypeface(tf);
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }

    public void getBalance() {
        try {
            String url = Constants.CORE_URL + Constants.BALANCE_SUFFIX;
            HashMap<String, String> list = new HashMap<String, String>();
            list.put("groupId", sharedPreferences.getString("groupId", ""));
            list.put("userId", sharedPreferences.getString("userid", ""));
            list.put("installationId", sharedPreferences.getString(Constants.PARSE_INSTALLATION_ID, ""));
            list.put(Constants.IMEI,
                    CommonUtility.getImeiNumber(MainActivity.this));
            list.put(Constants.DEVICE_ID,
                    CommonUtility.getDeviceId(MainActivity.this));
            list.put(Constants.EMAIL_ID,
                    CommonUtility.getEmailId(MainActivity.this));
            list.put(Constants.MOBILE_OS, "Android"
                    + Build.VERSION.RELEASE);

            list.put("mobileApp", "true");
            list.put("mobileAppVersion", CommonUtility.getVersionCode(MainActivity.this));
            serverCommunication(url, list, Constants.BALANCE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
