package spicedigital.in.npcimerchantapp.home_screen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;

import spicedigital.in.npcimerchantapp.Fragments.FragmentAddNewPayment;
import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.database.MyDataBase;

/**
 * Created by ch-e00812 on 3/19/2016.
 */
public class PaymentDetail extends AppCompatActivity implements View.OnClickListener{

    Toolbar toolbar;
    View view;
    TextView titleText;
    TextView event_name,amount,status;
    TextView raise_by,date,description,merchant_name,merchant_vpa,view_bill;
    Button reject,approve;
    HashMap<String, String> recordData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_detail);
        if(getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            recordData  = (HashMap<String, String>) bundle.getSerializable("recordData");
        }
        initialiseToolbar();
        initialiseviews();
        setRecordData();
    }

    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();
    }

    public void initialiseviews() {
   try {
       titleText = (TextView) view.findViewById(R.id.title_text);
       titleText.setText("PAYMENT DETAIL");

       event_name = (TextView) view.findViewById(R.id.event_name);
       amount = (TextView) view.findViewById(R.id.amount);
       status = (TextView) view.findViewById(R.id.status);

       raise_by = (TextView) view.findViewById(R.id.raise_by);
       date = (TextView) view.findViewById(R.id.date);
       description = (TextView) view.findViewById(R.id.description);
       merchant_name = (TextView) view.findViewById(R.id.merchant_name);
       merchant_vpa = (TextView) view.findViewById(R.id.merchant_vpa);
       view_bill = (TextView) view.findViewById(R.id.view_bill);

       reject = (Button) view.findViewById(R.id.reject);
       approve = (Button) view.findViewById(R.id.approve);
       reject.setOnClickListener(this);
       approve.setOnClickListener(this);
   }catch(Exception e){

   }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.reject) {

              finish();
        } else if (view.getId() == R.id.approve) {

            FragmentAddNewPayment fragmentAddNewPayment = new FragmentAddNewPayment();
              Bundle bundle= new Bundle();
              bundle.putSerializable("recordData",recordData);
              fragmentAddNewPayment.setArguments(bundle);
             fragmentAddNewPayment.show(getSupportFragmentManager(),"");
        }
    }
    public void setRecordData(){
        try{
            event_name.setText(recordData.get(MyDataBase.KEY_EVENT_NAME));
            raise_by.setText(recordData.get(MyDataBase.KEY_PAYER_NAME));
            status.setText(recordData.get(MyDataBase.KEY_CURRENT_STATUS));
            date.setText(recordData.get(MyDataBase.KEY_CURRENT_DATE));
            amount.setText(getResources().getString(R.string.rupees)+" "+recordData.get(MyDataBase.KEY_AMOUNT));

            description.setText(recordData.get(MyDataBase.KEY_DESCRIPTION));

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
