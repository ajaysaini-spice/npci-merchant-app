package spicedigital.in.npcimerchantapp.home_screen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import spicedigital.in.npcimerchantapp.R;

/**
 * Created by ch-e00812 on 3/19/2016.
 */
public class CalendarScreen extends AppCompatActivity {

    Toolbar toolbar;
    View view;
    TextView titleText;
    ImageView calendarView;

    Integer[] calendarArr = new Integer[]{
            R.drawable.calender,
            R.drawable.calender_big_19,
            R.drawable.calender_big_20,
            R.drawable.calender_big_21,
            R.drawable.calender_big_22,
            R.drawable.calender_big_23,
            R.drawable.calender_big_24,
            R.drawable.calender_big_25,
            R.drawable.calender_big_26,
            R.drawable.calender_big_27
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_screen);

        initialiseToolbar();
        initialiseviews();

    }

    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();
    }

    public void initialiseviews() {

        titleText = (TextView) view.findViewById(R.id.title_text);
        titleText.setText("CALENDAR");
        calendarView = (ImageView) findViewById(R.id.calenderView);

        setCalendarImage();

    }


    private void setCalendarImage(){

        DateFormat dateFormat = new SimpleDateFormat("dd");
        Date date = new Date();
        String todayDay = dateFormat.format(date);

        if (todayDay.equalsIgnoreCase("18")){
            calendarView.setImageResource(calendarArr[0]);
        }
        else if (todayDay.equalsIgnoreCase("19")){
            calendarView.setImageResource(calendarArr[1]);
        }
        else if (todayDay.equalsIgnoreCase("20")){
            calendarView.setImageResource(calendarArr[2]);
        }
        else if (todayDay.equalsIgnoreCase("21")){
            calendarView.setImageResource(calendarArr[3]);
        }
        else if (todayDay.equalsIgnoreCase("22")){
            calendarView.setImageResource(calendarArr[4]);
        }
        else if (todayDay.equalsIgnoreCase("23")){
            calendarView.setImageResource(calendarArr[5]);
        }
        else if (todayDay.equalsIgnoreCase("24")){
            calendarView.setImageResource(calendarArr[6]);
        }
        else if (todayDay.equalsIgnoreCase("25")){
            calendarView.setImageResource(calendarArr[7]);
        }
        else if (todayDay.equalsIgnoreCase("26")){
            calendarView.setImageResource(calendarArr[8]);
        }
        else if (todayDay.equalsIgnoreCase("27")){
            calendarView.setImageResource(calendarArr[9]);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
