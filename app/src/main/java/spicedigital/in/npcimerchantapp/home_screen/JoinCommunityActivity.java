package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.R;

public class JoinCommunityActivity extends AppCompatActivity implements View.OnClickListener {

    private Button cancelCommunityButton, btnJoinCommunity;
    private TextView txtViewDynamicCommunityName, txtViewCommunityType, textViewDescription;
    ImageView imageViewProfile;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_group_layout);
        try{
         sharedPreferences = PreferenceManager.getDefaultSharedPreferences(JoinCommunityActivity.this);
        cancelCommunityButton = (Button) findViewById(R.id.cancelCommunityButton);
        cancelCommunityButton.setOnClickListener(this);

        btnJoinCommunity = (Button) findViewById(R.id.btnJoinCommunity);


            imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);
        txtViewDynamicCommunityName = (TextView) findViewById(R.id.txtViewDynamicCommunityName);
        txtViewCommunityType = (TextView) findViewById(R.id.txtViewCommunityType);
        textViewDescription = (TextView) findViewById(R.id.textViewDescription);

        String Name = sharedPreferences.getString("communityType", "");
            String type = sharedPreferences.getString("communityName", "");

            if(!Name.equalsIgnoreCase("")) {
                txtViewDynamicCommunityName.setText(Name);
            }

            if(!type.equalsIgnoreCase("")) {
                txtViewCommunityType.setText(type);
            }

        btnJoinCommunity.setOnClickListener(this);
        cancelCommunityButton.setOnClickListener(this);

            setCategoryImage();
    }catch(Exception e){
        e.printStackTrace();
    }

    }

    private void setCategoryImage() {

        if (sharedPreferences.getString("communityType", "").equalsIgnoreCase("Housing Society")) {
            imageViewProfile.setImageResource(R.drawable.housing);
        } else if (sharedPreferences.getString("communityType", "").equalsIgnoreCase("NGO")) {
            imageViewProfile.setImageResource(R.drawable.ngo);

        } else if (sharedPreferences.getString("communityType", "").equalsIgnoreCase("School")) {
            imageViewProfile.setImageResource(R.drawable.school);

        } else if (sharedPreferences.getString("communityType", "").equalsIgnoreCase("Self help group")) {
            imageViewProfile.setImageResource(R.drawable.self);

        } else if (sharedPreferences.getString("communityType", "").equalsIgnoreCase("Microsofinance industry")) {
            imageViewProfile.setImageResource(R.drawable.finance);

        } else if (sharedPreferences.getString("communityType", "").equalsIgnoreCase("Micro insurance provider")) {
            imageViewProfile.setImageResource(R.drawable.insurance);

        } else if (sharedPreferences.getString("communityType", "").equalsIgnoreCase("NBFC")) {
            imageViewProfile.setImageResource(R.drawable.nbfc);

        }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cancelCommunityButton) {
            finish();

        } else if (view.getId() == R.id.btnJoinCommunity) {



            startActivity(new Intent(JoinCommunityActivity.this, JoinGroupDetailActivity.class));
        }
    }
}
