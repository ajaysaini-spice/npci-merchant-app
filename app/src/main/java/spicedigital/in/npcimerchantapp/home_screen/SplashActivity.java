package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.Constants;

/**
 * Created by CH-E01062 on 19-03-2016.
 */
public class SplashActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_view);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        new Handler().postDelayed(new Runnable() {

			/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                if(sharedPreferences.getBoolean(Constants.isMainActivityOpened, false))
                {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
                else
                {
                    Intent i = new Intent(SplashActivity.this, OtpActivity.class);
                    startActivity(i);

                    // close this activity
                    finish();
                }

            }
        }, 3000);
    }
}
