package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.utils.CheckInternetConnection;
import spicedigital.in.npcimerchantapp.utils.CustomDialogNetworkRequest;
import spicedigital.in.npcimerchantapp.utils.VolleyResponse;

public class AccountLinking extends AppCompatActivity implements VolleyResponse {

    CustomDialogNetworkRequest requestClass;
    AutoCompleteTextView editTextLinkingType;
    EditText edittextAcc,editTextCustomerId,editTextVPA;
    CheckBox checkViewvpa,checkViewnewvpa;
    Button btnSubmit;
    Context context;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_linking);
        context = this;
        intializeViews();
    }

public void intializeViews(){
    try{

        edittextAcc = (EditText) findViewById(R.id.edittextAcc);
        editTextCustomerId = (EditText) findViewById(R.id.editTextCustomerId);
        editTextVPA = (EditText) findViewById(R.id.editTextVPA);
        editTextLinkingType = (AutoCompleteTextView) findViewById(R.id.editTextLinkingType);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        checkViewvpa = (CheckBox) findViewById(R.id.checkViewvpa);
        checkViewnewvpa = (CheckBox) findViewById(R.id.checkViewnewvpa);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);


        checkViewvpa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkViewvpa.setVisibility(View.VISIBLE);
                checkViewnewvpa.setVisibility(View.GONE);

            }
        });

        checkViewnewvpa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkViewvpa.setVisibility(View.VISIBLE);
                checkViewnewvpa.setVisibility(View.VISIBLE);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(Validation()){
                   accountLinking();
               }
            }
        });
    }catch(Exception e){
        e.printStackTrace();
    }
}

    public Boolean Validation(){
        try{
            if (edittextAcc.getText().toString().length() == 0) {
                CommonUtility.showSimpleSnackBar(context, "Please enter your ICICI Account Number", coordinatorLayout);
            } else if (editTextCustomerId.getText().toString().length() == 0) {
                CommonUtility.showSimpleSnackBar(context, "Please enter Customer ID", coordinatorLayout);
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return true;
    }
    public void accountLinking(){
        try{
            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AccountLinking.this);
            final String mobileNumber = sharedPreferences.getString(Constants.mobileNumber, "");



            String url=Constants.CORE_URL+Constants.ACCOUNTLINKING_SUFFIX;

            HashMap<String, String> list = new HashMap<String, String>();

            list.put("custId", editTextCustomerId.getText().toString());
            list.put("communityVpa", editTextVPA.getText().toString().trim());
            list.put("accountNo", edittextAcc.getText().toString().trim());
            list.put("linking", editTextLinkingType.getText().toString().trim());

            list.put("msisdn", mobileNumber);
            list.put("type", "ADMIN");
            list.put("permission", "");
            list.put("installationId", sharedPreferences.getString(Constants.PARSE_INSTALLATION_ID,""));


            list.put(Constants.IMEI,
                    CommonUtility.getImeiNumber(AccountLinking.this));
            list.put(Constants.DEVICE_ID,
                    CommonUtility.getDeviceId(AccountLinking.this));
            list.put(Constants.EMAIL_ID,
                    CommonUtility.getEmailId(AccountLinking.this));
            list.put(Constants.MOBILE_OS, "Android"
                    + Build.VERSION.RELEASE);
            list.put("mobileApp", "true");
            list.put("mobileAppVersion", CommonUtility.getVersionCode(AccountLinking.this));


            serverCommunication(url,list,Constants.ACCOUNTLINKING);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void serverCommunication(String URL, HashMap<String, String> list,String resultCode) {


        if (CheckInternetConnection.haveNetworkConnection(AccountLinking.this)) {
            try {
                requestClass = new CustomDialogNetworkRequest(AccountLinking.this, this);
                if (requestClass == null) {
                    requestClass = new CustomDialogNetworkRequest(AccountLinking.this, this);

                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                } else {
                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                }
            } catch (Exception e) {

            }


        } else {
            CommonUtility.showNointernetDialog(AccountLinking.this);
        }
    }

    @Override
    public void onResult(String result, String responseCode) {
        if (responseCode.equalsIgnoreCase(Constants.ACCOUNTLINKING)) {
            try{
                if(!result.equalsIgnoreCase("") && result != null) {
                    JSONObject responsejsonData = new JSONObject(result);
                    String response = responsejsonData.optString("response");

                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.optString("status");
                    if(status.equalsIgnoreCase("S")){
                        String message= jsonData.optString("description");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();




                        startActivity(new Intent(AccountLinking.this, ActivityCreateCommunity.class));
                    }else{

                        String message= jsonData.optString("description");
                        CommonUtility.showAlert_dialogue(message,true,AccountLinking.this);
                    }



                }else{

                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
