package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.Fragments.FragmentAddNewPayment;
import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.PaymentRequestiesListAdapter;
import spicedigital.in.npcimerchantapp.application.AppControler;
import spicedigital.in.npcimerchantapp.database.MyDataBase;

/**
 * Created by CH-E01062 on 18-03-2016.
 */
public class PaymentRequestiesActivity extends AppCompatActivity implements View.OnClickListener {

    ListView mListView;
    PaymentRequestiesListAdapter adapter;
    ArrayList mDataList;
    Toolbar toolbar;
    View view;
    TextView titleText;
    FrameLayout allPayments, forApproval;
    ImageView fabButton;
    ArrayList<HashMap<String, String>> getpaymentrequest;
    String PersonType="";
    ArrayList<HashMap<String, String>> getpayrequestformembers;

    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_requisties);
        initialiseToolbar();


        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

         PersonType = sharedPreferences.getString("PersonType","");

        initialiseviews();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                HashMap<String, String> obj = (HashMap<String, String>) mListView.getAdapter().getItem(position);
                Intent intent = new Intent(PaymentRequestiesActivity.this, PaymentDetail.class);

                intent.putExtra("recordData",obj);
                startActivity(intent);

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{

            if (PersonType.equalsIgnoreCase("ADMIN")) {
                getpaymentrequest = new ArrayList<HashMap<String, String>>();

                getpaymentrequest = AppControler.getDataBaseInstance().getNotificationList();
                if(getpaymentrequest.size() > 0) {
                    adapter = new PaymentRequestiesListAdapter(this, getpaymentrequest);
                    mListView.setAdapter(adapter);
                }
            } else {
                getpayrequestformembers = new ArrayList<HashMap<String, String>>();
                getpayrequestformembers = AppControler.getDataBaseInstance().getDatafromRequests(MyDataBase.TABLE_PAYMEMBERAPPROVAL);
                if (getpayrequestformembers != null && getpayrequestformembers.size() > 0) {

                    adapter = new PaymentRequestiesListAdapter(this, getpayrequestformembers);
                    mListView.setAdapter(adapter);

                }

            }

        }catch(Exception e){

        }
    }

    public void initialiseviews() {
        try {


            allPayments = (FrameLayout) findViewById(R.id.all_payments);
            allPayments.setOnClickListener(this);
            forApproval = (FrameLayout) findViewById(R.id.for_approval);
            forApproval.setOnClickListener(this);

            fabButton = (ImageView) findViewById(R.id.fab_btn);
            fabButton.setOnClickListener(this);
            titleText = (TextView) view.findViewById(R.id.title_text);
            titleText.setText("PAYMENT REQUESTS");
            if(PersonType.equalsIgnoreCase("ADMIN")){
                titleText.setText("PAYMENT REQUESTS");
            }else{
                titleText.setText("INITIATE PAYMENTS");

            }
            mDataList = new ArrayList();
            mDataList.add("1");
            mDataList.add("1");
            mDataList.add("1");
            mDataList.add("1");
            mListView = (ListView) findViewById(R.id.payment_requisties_list);




        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        if (v == fabButton) {

            FragmentAddNewPayment payment = new FragmentAddNewPayment();
            payment.show(getSupportFragmentManager(), "");

        } else if (v == forApproval) {

            forApproval.setBackgroundResource(R.drawable.tab_button_selected);

            allPayments.setBackgroundResource(R.drawable.tab_button_un_selected);

        } else if (v == allPayments) {

            forApproval.setBackgroundResource(R.drawable.tab_button_un_selected);

            allPayments.setBackgroundResource(R.drawable.tab_button_selected);

        }


    }
}