package spicedigital.in.npcimerchantapp.home_screen;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.utils.CheckInternetConnection;
import spicedigital.in.npcimerchantapp.utils.CustomDialogNetworkRequest;
import spicedigital.in.npcimerchantapp.utils.VolleyResponse;

public class JoinGroupDetailActivity extends AppCompatActivity implements View.OnClickListener,VolleyResponse {

    CustomDialogNetworkRequest requestClass;
    Button btnSubmit;
    private EditText edittextName, editTextVPA, editTextAccountNo, editTextMMID, editTextIFCCode, editTextAdharCodeNo;
    private SharedPreferences sharedPreferences;
    private Context context;
    RadioButton male, female;
    private TextView editTextDOB;
    public boolean flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_group_details);
        context = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        initializeViews();
        setOnClickListener();

    }

    private void setOnClickListener() {
        btnSubmit.setOnClickListener(this);
        editTextDOB.setOnClickListener(this);
    }

    public void initializeViews() {
        try {
            male = (RadioButton) findViewById(R.id.male);
            female = (RadioButton) findViewById(R.id.female);
            male.setChecked(true);

            male.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    female.setChecked(false);
                    male.setChecked(true);
                    flag = false;

                }
            });
            female.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flag = true;
                    female.setChecked(true);
                    male.setChecked(false);
                }
            });





            btnSubmit = (Button) findViewById(R.id.btnSubmit);
            edittextName = (EditText) findViewById(R.id.edittextName);
            editTextDOB = (TextView) findViewById(R.id.editTextDOB);
            editTextVPA = (EditText) findViewById(R.id.editTextVPA);
            editTextAccountNo = (EditText) findViewById(R.id.editTextAccountNo);
            editTextMMID = (EditText) findViewById(R.id.editTextMMID);
            editTextIFCCode = (EditText) findViewById(R.id.editTextIFCCode);
            editTextAdharCodeNo = (EditText) findViewById(R.id.editTextAdharCodeNo);
        } catch (Exception e) {

        }

    }

    @Override
    public void onClick(View v) {
        if (v == btnSubmit) {


            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(JoinGroupDetailActivity.this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("memberAcc", editTextAccountNo.getText().toString());
            editor.putString("membername", edittextName.getText().toString());
            editor.putString("memberVPA", editTextVPA.getText().toString());

            editor.putString("PersonType", "MEMBER");
            editor.commit();

            if (edittextName.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your First Name", Toast.LENGTH_SHORT).show();
            } /*else if (editTextDOB.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your Date Of Birth", Toast.LENGTH_SHORT).show();
            } else if (editTextGender.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your Gender", Toast.LENGTH_SHORT).show();
            } */ else if (editTextVPA.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your VPA", Toast.LENGTH_SHORT).show();
            } else if (editTextAccountNo.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your Account Number", Toast.LENGTH_SHORT).show();
            } /*else if (editTextMMID.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your MMID", Toast.LENGTH_SHORT).show();
            } else if (editTextIFCCode.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your IFC Code", Toast.LENGTH_SHORT).show();
            } */ else {
                /*sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("First name", edittextName.getText().toString());
                edit.putString("Date Of Birth", editTextDOB.getText().toString());
                edit.putString("Gender", "");
                edit.putString("VPA", editTextVPA.getText().toString());
                edit.putString("Account Number", editTextAccountNo.getText().toString());
                edit.putString("MMID", editTextMMID.getText().toString());
                edit.putString("IFC Code", editTextIFCCode.getText().toString());
                edit.commit();

                startActivity(new Intent(JoinGroupDetailActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                */

                joinGroup();
            }
        } else if (v == editTextDOB) {
            DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {

                    Calendar myCalendar = Calendar.getInstance();
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    myCalendar.set(Calendar.YEAR, year);

                    updatedate(year, monthOfYear, dayOfMonth);


                }
            };

            Calendar myCalendar = Calendar.getInstance();

            new DatePickerDialog(context, d,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    }

    protected void updatedate(int yr, int mon, int day) {
        try {


            Date toDate, fromDate = null;


            String dtStart = yr + "/" + (mon + 1) + "/" + day;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            try {
                fromDate = format.parse(dtStart);

                dtStart = format.format(fromDate);

            } catch (Exception e) {
                // TODO Auto-generated catch block

            }


            editTextDOB.setText(dtStart);


        } catch (Exception e) {

        }
    }

    public void joinGroup(){
        try{
            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(JoinGroupDetailActivity.this);
            final String mobileNumber = sharedPreferences.getString(Constants.mobileNumber, "");

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("First name", edittextName.getText().toString());
            editor.putString("Date Of Birth", editTextDOB.getText().toString());
            editor.putString("Gender", "");
            editor.putString("VPA", editTextVPA.getText().toString());
            editor.putString("Account Number", editTextAccountNo.getText().toString());
            editor.putString("MMID", editTextMMID.getText().toString());
            editor.putString("IFC Code", editTextIFCCode.getText().toString());

            editor.putString("memberAcc", editTextAccountNo.getText().toString());
            editor.putString("membername", edittextName.getText().toString());
            editor.putString("memberVPA", editTextVPA.getText().toString());
            editor.putString("PersonType", "MEMBER");
            editor.commit();



            String url=Constants.CORE_URL+Constants.JOINMEMBER_SUFFIX;

            HashMap<String, String> list = new HashMap<String, String>();

            list.put("firstName", edittextName.getText().toString());
            list.put("lastName", "");
            list.put("dob", editTextDOB.getText().toString().trim());
            list.put("gender", "");

            list.put("vpa", editTextVPA.getText().toString().trim());
            list.put("accountNo", editTextAccountNo.getText().toString().trim());
            list.put("msisdn", mobileNumber);
            list.put("type", "MEMBER");
            list.put("mmid", editTextMMID.getText().toString());
            list.put("ifscCode", editTextIFCCode.getText().toString());
            list.put("adharCard", editTextAdharCodeNo.getText().toString());
            list.put("groupId", sharedPreferences.getString("groupId",""));
            list.put("referCode", sharedPreferences.getString("userid",""));
            list.put("installationId", sharedPreferences.getString(Constants.PARSE_INSTALLATION_ID,""));


            list.put(Constants.IMEI,
                    CommonUtility.getImeiNumber(JoinGroupDetailActivity.this));
            list.put(Constants.DEVICE_ID,
                    CommonUtility.getDeviceId(JoinGroupDetailActivity.this));
            list.put(Constants.EMAIL_ID,
                    CommonUtility.getEmailId(JoinGroupDetailActivity.this));
            list.put(Constants.MOBILE_OS, "Android"
                    + Build.VERSION.RELEASE);
            list.put("mobileApp", "true");
            list.put("mobileAppVersion", CommonUtility.getVersionCode(JoinGroupDetailActivity.this));


            serverCommunication(url,list,Constants.JOINMEMBER);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void serverCommunication(String URL, HashMap<String, String> list,String resultCode) {


        if (CheckInternetConnection.haveNetworkConnection(JoinGroupDetailActivity.this)) {
            try {
                requestClass = new CustomDialogNetworkRequest(JoinGroupDetailActivity.this, this);
                if (requestClass == null) {
                    requestClass = new CustomDialogNetworkRequest(JoinGroupDetailActivity.this, this);

                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                } else {
                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                }
            } catch (Exception e) {

            }


        } else {
            CommonUtility.showNointernetDialog(JoinGroupDetailActivity.this);
        }
    }

    @Override
    public void onResult(String result, String responseCode) {
        if (responseCode.equalsIgnoreCase(Constants.JOINMEMBER)) {
            try{
                if(!result.equalsIgnoreCase("") && result != null) {
                    JSONObject responsejsonData = new JSONObject(result);
                    String response = responsejsonData.optString("response");

                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.optString("status");
                    if(status.equalsIgnoreCase("S")){
                        String message= jsonData.optString("description");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        String groupDetailPojo = jsonData.optString("groupDetailPojo");
                        JSONObject userData = new JSONObject(groupDetailPojo);
                        String groupId = userData.optString("groupId");
                        String adminId = userData.optString("adminId");
                        String userId = userData.optString("userId");

                        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(JoinGroupDetailActivity.this);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("PersonType","MEMBER");
                        editor.putString("userid",userId.trim());
                        editor.putString("adminId",adminId.trim());
                        editor.putString("groupId", groupId.trim());
                        editor.commit();

                        startActivity(new Intent(JoinGroupDetailActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));


                    }else{

                        String message= jsonData.optString("description");
                        CommonUtility.showAlert_dialogue(message,true,JoinGroupDetailActivity.this);
                    }



                }else{

                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}