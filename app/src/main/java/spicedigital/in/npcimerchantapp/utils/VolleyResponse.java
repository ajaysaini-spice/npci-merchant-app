package spicedigital.in.npcimerchantapp.utils;

public interface VolleyResponse {

    public void onResult(String result, String responseCode);

}
