package spicedigital.in.npcimerchantapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.Constants;


/**
 * Created by CH-E01073 on 10-02-2016.
 */

public class CustomDialogNetworkRequest {


    VolleyResponse listener;
    Context mContext;
    VolleyResponse resultContext;
    HashMap<String, String> params;
    String requestCode;

    /*public NetworkRequestClass(VolleyResponse context) {

        params = new HashMap<>();

    }*/

    public CustomDialogNetworkRequest(VolleyResponse resultContet, Context context) {
        mContext = context;
        resultContext = resultContet;

    }


    public void makePostRequest(String url, final Boolean showDialog, HashMap<String, String> parameters, final String responseCode,String dialogMsg) {

       // final ProgressBarHandler mProgressBarHandler = new ProgressBarHandler(mContext); // In onCreate
       // final Dialog mOverlayDialog = new Dialog(mContext, android.R.style.Theme_Panel); //display an invisible overlay dialog to prevent user interaction and pressing back
       // mOverlayDialog.setCancelable(false);

        final Dialog mOverlayDialog = new Dialog(mContext);
        mOverlayDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mOverlayDialog.setCancelable(false);
        mOverlayDialog.setContentView(R.layout.dialog_railloader);
        final TextView txt_upperlabel = (TextView) mOverlayDialog
                .findViewById(R.id.txt_upperlabel);
        txt_upperlabel.setText("This may take some time depending on your connection speed");
        final ImageView imgs = (ImageView) mOverlayDialog
                .findViewById(R.id.imgs);

        final ImageView image_railprogressdilaog = (ImageView) mOverlayDialog
                .findViewById(R.id.image_railprogressdilaog);
        image_railprogressdilaog.setVisibility(View.INVISIBLE);
        startAnimation(imgs);

        requestCode = responseCode;

        if (showDialog == true) {

            try {
                mOverlayDialog.show();
            } catch (Exception e) {

            }


            try {
                //mProgressBarHandler.show(); // To show the progress bar*/
            } catch (Exception e) {

            }

        }


        params = parameters;



        listener = resultContext;

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest requestPost = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (listener != null) {
                            Log.e("Response", response);
                            listener.onResult(response, responseCode);
                            if (showDialog == true) {
                                try {
                                    mOverlayDialog.dismiss();
                                } catch (Exception e) {

                                }

                                try {
                                  //  mProgressBarHandler.hide(); // To hide the progress bar
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (listener != null) {

                    listener.onResult(error.toString(), Constants.VOLLEY_ERROR);

                }
                if (showDialog == true) {
                    try {
                        mOverlayDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                       // mProgressBarHandler.hide(); // To hide the progress bar
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


        }) {

            @Override
            protected Map<String, String> getParams() {


                return checkParams(params);
            }



			/*
             * @Override public Map<String, String> getHeaders() throws
			 * AuthFailureError { Map<String, String> params = new
			 * HashMap<String, String>(); params.put("Content-Type",
			 * "application/x-www-form-urlencoded"); return params; }
			 */
        };
        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
//Volley does retry for you if you have specified the policy.


        requestPost.setRetryPolicy(new DefaultRetryPolicy(120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        //checking if internet connection is available
        try {
            if (CheckInternetConnection.haveNetworkConnection(mContext)) {

                queue.add(requestPost);

            } else {
                if (showDialog == true) {
                    try {
                        mOverlayDialog.dismiss();
                    } catch (Exception e) {

                    }

                    try {
                        //mProgressBarHandler.hide(); // To hide the progress bar
                    } catch (Exception e) {

                    }

                }

                //  AlertDialogManager.showAlertDialog(mContext, mContext.getResources().getString(R.string.no_internet_title), mContext.getResources().getString(R.string.no_internet_message));
                listener.onResult("No Connection Available", Constants.NO_CONNECTION);


            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }


    protected void startAnimation(ImageView view){
        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f , Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(2000);
        view.setAnimation(anim);
        view.startAnimation(anim);
    }
}
