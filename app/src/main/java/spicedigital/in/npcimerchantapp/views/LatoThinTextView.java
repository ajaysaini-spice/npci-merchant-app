package spicedigital.in.npcimerchantapp.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by CH-E01073 on 04-12-2015.
 */

public class LatoThinTextView extends TextView {

    public LatoThinTextView(Context context) {
        super(context);
        style(context);
    }

    public LatoThinTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public LatoThinTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        try {
            Typeface tf = Typeface.createFromAsset(context.getAssets(),
                    "fonts/Lato-Light.ttf");
            setTypeface(tf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
