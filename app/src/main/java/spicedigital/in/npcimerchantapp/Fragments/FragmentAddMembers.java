package spicedigital.in.npcimerchantapp.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.AddMembersAdapter;
import spicedigital.in.npcimerchantapp.home_screen.MainActivity;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.support_classes.Contact;
import spicedigital.in.npcimerchantapp.utils.CheckInternetConnection;
import spicedigital.in.npcimerchantapp.utils.CustomDialogNetworkRequest;
import spicedigital.in.npcimerchantapp.utils.VolleyResponse;

/**
 * Created by ch-e00812 on 3/19/2016.
 */
public class FragmentAddMembers extends DialogFragment implements VolleyResponse {


    Context context;
    View rootView;
    CustomDialogNetworkRequest requestClass;
    RecyclerView recyclerView;

    Button btnCancel, btnAdd;

    LinearLayout progressBarLay;

    public static Handler checkBoxHandler;

    //    String checkBoxCount;
    ArrayList<String> checkedPosList;

    public FragmentAddMembers() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_member, container,
                false);
        context = getActivity();
        initilizeView(rootView);

        checkedPosList = new ArrayList<>();

        new FetchContactTask().execute();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return rootView;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        try {
            dialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
        }
        return dialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            // safety check
            if (getDialog() == null) {
                return;
            }

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;

            getDialog().getWindow().setLayout((int) (width * .98f), ViewGroup.LayoutParams.WRAP_CONTENT);


        } catch (Exception e) {
        }
    }


    private void initilizeView(View mrootView) {

        recyclerView = (RecyclerView) mrootView.findViewById(R.id.recyclerView);
        btnCancel = (Button) mrootView.findViewById(R.id.btnCancel);
        btnAdd = (Button) mrootView.findViewById(R.id.btnAdd);
        progressBarLay = (LinearLayout) mrootView.findViewById(R.id.progressBarLay);
    }


    private ArrayList<Contact> readContacts(Context context) throws Exception {

        ArrayList<String> tempAlist = new ArrayList<>();

        ArrayList<Contact> contactArrayList = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String image_uri = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred        = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:

                                String tempNo = phoneNo.replace("+91", "").replace(" ", "").replace("-", "").replaceAll("[^0-9]+", "");

                                if (!tempAlist.contains(tempNo)) {
                                    tempAlist.add(tempNo);

                                    Contact contact = new Contact();
                                    contact.setName(contactName);
                                    contact.setNumber(tempNo);
                                    contact.setImageURI(image_uri);
                                    contactArrayList.add(contact);
//                                    HashSet<Contact> mSet = new HashSet<>();
//                                    mSet.addAll(contactArrayList);
//                                    contactArrayList.clear();
//                                    contactArrayList.addAll(mSet);
                                }

                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();
        }
        return contactArrayList;
    }


    private class FetchContactTask extends AsyncTask<Void, Void, ArrayList<Contact>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBarLay.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

        }

        @Override
        protected ArrayList<Contact> doInBackground(Void... params) {
            try {
                ArrayList<Contact> contactArrayList = new ArrayList<Contact>();
                contactArrayList = readContacts(context);
                return contactArrayList;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(final ArrayList<Contact> result) {
            super.onPostExecute(result);

            progressBarLay.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            if (result != null) {
                AddMembersAdapter addMembersAdapter = new AddMembersAdapter(context, result);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(addMembersAdapter);


                checkBoxHandler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);

                        if (msg.what == 1000) {
                            checkedPosList = (ArrayList<String>) msg.obj;
                            btnAdd.setText("ADD " + checkedPosList.size() + " MEMBERS");
                        }

                    }
                };


                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            ArrayList<String> arrayList = new ArrayList<String>();

                            for (int i = 0; i < checkedPosList.size(); i++) {

                                String selectedNumber = result.get(Integer.parseInt(checkedPosList.get(i))).getNumber();
                                selectedNumber = selectedNumber.replace("-", "");
                                selectedNumber = selectedNumber.replace(" ", "");

                                if (selectedNumber.length() == 0) {

                                    AlertDialog.Builder alert2 = new AlertDialog.Builder(
                                            getActivity());
                                    alert2.setMessage("No Mobile number found");
                                    alert2.setPositiveButton("OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {

                                                }
                                            });
                                    alert2.setCancelable(true);
                                    alert2.show();

                                    // showDialog("No Mobile number found");
                                } else if (selectedNumber.length() > 10) {
                                    selectedNumber = selectedNumber
                                            .substring(selectedNumber.length() - 10);
                                } else {

                                    //saveDataToParse(result.get(Integer.parseInt(checkedPosList.get(i))).getName(), selectedNumber);

                                    arrayList.add(selectedNumber);

                                }
                            }

                            addMembersGroup(arrayList);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                });


            }


        }

        private void saveDataToParse(final String memberName, final String mobileNumber) {
        /*ParseQuery<ParseObject> queryUserAccount = new ParseQuery<ParseObject>(
                "Admin");
        queryUserAccount.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null) {*/

            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());


            ParseQuery<ParseObject> queryUserAccount = new ParseQuery<ParseObject>(
                    "Member");
            queryUserAccount.whereEqualTo("mobileNumber",
                    mobileNumber);
            queryUserAccount.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if (e == null) {


                    } else {
                        ParseObject member = new ParseObject("Member");

                        if (mobileNumber != null && mobileNumber.length() > 0) {
                            member.put("mobileNumber", mobileNumber);
                            member.put("memberName", memberName);
                            member.put("communityType", sharedPreferences.getString("communityType", ""));
                            member.put("communityName", sharedPreferences.getString("communityName", ""));
                            member.put("communityVPA", sharedPreferences.getString("communityVPA", ""));
                            member.put("adminMobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));
                            member.saveInBackground();
                        }
                    }
                }
            });


            // String mobileNumber = sharedPreferences.getString(Constants.mobileNumber, "");

           /* parseObject.put("communityName", editTextCommunityName.getText().toString());
            parseObject.put("name", edittextName.getText().toString());
            parseObject.put("communityType", editTextCommunityType.getText().toString());
            parseObject.put("communityDesc", editTextDesc.getText().toString());
            parseObject.put("communityVPA", editTextVPA.getText().toString());
            parseObject.put("communityId", editTextCommunityName.getText().toString() + new Random().nextInt());*/

              /*  }
            }
        });*/
        }
    }


    public void addMembersGroup(ArrayList<String> arrayList){
        try{
            String arryofnum="";
            for (int i = 0; i < arrayList.size(); i++) {
                if (i == arrayList.size() - 1) {
                    arryofnum = arryofnum + arrayList.get(i);
                } else {
                arryofnum = arryofnum + arrayList.get(i) + ",";
                 }
            }


            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String url=Constants.CORE_URL+Constants.INVITEMEMBER_SUFFIX;

            HashMap<String, String> list = new HashMap<String, String>();


            list.put("msisdnarray", arryofnum);
            list.put("type", "ADMIN");
            list.put("groupId", sharedPreferences.getString("groupId",""));
            list.put("referCode", sharedPreferences.getString("userid",""));
            list.put("installationId", sharedPreferences.getString(Constants.PARSE_INSTALLATION_ID,""));


            list.put(Constants.IMEI,
                    CommonUtility.getImeiNumber(context));
            list.put(Constants.DEVICE_ID,
                    CommonUtility.getDeviceId(context));
            list.put(Constants.EMAIL_ID,
                    CommonUtility.getEmailId(context));
            list.put(Constants.MOBILE_OS, "Android"
                    + Build.VERSION.RELEASE);
            list.put("mobileApp", "true");
            list.put("mobileAppVersion", CommonUtility.getVersionCode(context));


            serverCommunication(url,list,Constants.INVITEMEMBER);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void serverCommunication(String URL, HashMap<String, String> list,String resultCode) {

        if (CheckInternetConnection.haveNetworkConnection(context)) {
            try {
                requestClass = new CustomDialogNetworkRequest(FragmentAddMembers.this, context);
                if (requestClass == null) {
                    requestClass = new CustomDialogNetworkRequest(FragmentAddMembers.this, context);

                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                } else {
                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                }
            } catch (Exception e) {

            }


        } else {
            CommonUtility.showNointernetDialog(context);
        }
    }

    @Override
    public void onResult(String result, String responseCode) {
        if (responseCode.equalsIgnoreCase(Constants.INVITEMEMBER)) {
            try{
                if(!result.equalsIgnoreCase("") && result != null) {
                    JSONObject responsejsonData = new JSONObject(result);
                    String response = responsejsonData.optString("response");

                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.optString("status");
                    if(status.equalsIgnoreCase("S")){
                        String message= jsonData.optString("description");
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                       /* String userInfo = jsonData.optString("userInfo");
                        JSONObject userData = new JSONObject(userInfo);
                        String userid = userData.optString("userid");
                        String groupId = userData.optString("groupId");

                        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ActivityCreateCommunity.this);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("userid",userid.trim());
                        editor.putString("groupId", groupId.trim());
                        editor.commit();*/

                        dismiss();
                        startActivity(new Intent(context, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }else{

                        String message= jsonData.optString("description");
                        CommonUtility.showAlert_dialogue(message,true,context);
                    }



                }else{

                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

}
