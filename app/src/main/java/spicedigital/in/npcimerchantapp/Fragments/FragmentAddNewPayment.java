package spicedigital.in.npcimerchantapp.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.application.AppControler;
import spicedigital.in.npcimerchantapp.database.MyDataBase;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.utils.CheckInternetConnection;
import spicedigital.in.npcimerchantapp.utils.CustomDialogNetworkRequest;
import spicedigital.in.npcimerchantapp.utils.VolleyResponse;

/**
 * Created by CH-E01039 on 3/19/2016.
 */
public class FragmentAddNewPayment extends DialogFragment implements View.OnClickListener,VolleyResponse {

    CustomDialogNetworkRequest requestClass;
    Context context;
    View rootView;
    EditText editTextEventName, editTextAmount, editTextMerchantName, editTextVPA, editTextDescription;
    TextView textDate;
    Button btnUploadBill, btnCancel, btnSave;
    HashMap<String, String> recordData;
    String PersonType="";
    Boolean memberpayfromownaccount=false;
    SharedPreferences sharedPreferences;

    public FragmentAddNewPayment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_new_payment, container,
                false);
        try {

            context = getActivity();
            initilizeView(rootView);

             sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context.getApplicationContext());

            PersonType = sharedPreferences.getString("PersonType","");

            if (getArguments().getSerializable("recordData") != null) {
                recordData = (HashMap<String, String>) getArguments().getSerializable("recordData");
                memberpayfromownaccount=true;
                  setData();
            }

        }catch(Exception e){
            e.printStackTrace();
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = getActivity();
    }

    public void setData(){
        try{
            editTextEventName.setText(recordData.get(MyDataBase.KEY_EVENT_NAME));
            textDate.setText(recordData.get(MyDataBase.KEY_EVENT_TIMESTAMP));
            editTextAmount.setText(recordData.get(MyDataBase.KEY_AMOUNT));
            editTextDescription.setText(recordData.get(MyDataBase.KEY_DESCRIPTION));


        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        try {
            dialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
        }
        return dialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            // safety check
            if (getDialog() == null) {
                return;
            }

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;

            getDialog().getWindow().setLayout((int) (width * .98f), ViewGroup.LayoutParams.WRAP_CONTENT);


        } catch (Exception e) {
        }
    }


    private void initilizeView(View mrootView) {
        editTextEventName = (EditText) mrootView.findViewById(R.id.editTextEventName);
        editTextDescription = (EditText) mrootView.findViewById(R.id.editTextDescription);
        editTextAmount = (EditText) mrootView.findViewById(R.id.editTextAmount);
        editTextMerchantName = (EditText) mrootView.findViewById(R.id.editTextMerchantName);
        editTextVPA = (EditText) mrootView.findViewById(R.id.editTextVPA);
        btnUploadBill = (Button) mrootView.findViewById(R.id.btnUploadBill);
        btnCancel = (Button) mrootView.findViewById(R.id.btnCancel);
        btnSave = (Button) mrootView.findViewById(R.id.btnSave);
        textDate = (TextView) mrootView.findViewById(R.id.textDate);
        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        textDate.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.textDate) {
            DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {

                    Calendar myCalendar = Calendar.getInstance();
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    myCalendar.set(Calendar.YEAR, year);

                    updatedate(year, monthOfYear, dayOfMonth);


                }
            };

            Calendar myCalendar = Calendar.getInstance();

            new DatePickerDialog(context, d,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();

        } else if (view.getId() == R.id.btnSave) {
            if (editTextMerchantName.getText().toString().length() == 0) {
                Toast.makeText(context, "Enter your Payee Name", Toast.LENGTH_SHORT).show();
            }else if(editTextVPA.getText().toString().length() == 0){
                Toast.makeText(context, "Enter your Payee VPA", Toast.LENGTH_SHORT).show();
            }else if(editTextAmount.getText().toString().length() == 0){
                Toast.makeText(context, "Enter your amount", Toast.LENGTH_SHORT).show();
            }else if(editTextEventName.getText().toString().length() == 0){
                Toast.makeText(context, "Enter your event name", Toast.LENGTH_SHORT).show();
            }else{
                if(appInstalledOrNot("spicedigital.in.npcipspserver")) {
                    if (PersonType.equalsIgnoreCase("ADMIN")) {
                        sendRequesToPSPAppforAdmintoMerchant();
                    } else {
                        if (memberpayfromownaccount) {
                            sendRequesToPSPAppforMemberAccounttoSociety();
                        } else {
                            sendPayRequestforMemberApproval();

                        }
                    }
                } else {
                    Toast.makeText(context, "Please install Spice Mudra PSP App.", Toast.LENGTH_LONG).show();
                }

            }

        } else if (view.getId() == R.id.btnCancel) {
            dismiss();
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public void sendRequesToPSPAppforAdmintoMerchant() {
        String DEEPLINKING_URL_BASE = "upi://pay";
        StringBuilder urlBuilder = new StringBuilder();
        int orderId = 100001 % new Random().nextInt();


        JSONObject payload2 = new JSONObject();
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            payload2.put("amount",editTextAmount.getText().toString().trim());
            payload2.put("payeeName", editTextMerchantName.getText().toString().trim());
            payload2.put("payeeAddress", editTextVPA.getText().toString().trim());

                payload2.put("payerAddress", sharedPreferences.getString("communityVPA", ""));
                payload2.put("payerName", sharedPreferences.getString("name", ""));

            payload2.put("mobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));
        } catch (Exception e) {

        }

        //   urlBuilder.append("upi://pay").append("?").append("amount").append("=").append("100").append("&").append("ORDERID").append("=").append(Integer.parseInt("100")).append("&").append("credit_flag").append("=").append("1").append("&").append("appname").append("=").append("MGSAPP").append("&").append("am").append("=").append("100").append("&").append("tn").append("=").append(Integer.parseInt("100")).append("&").append("tr").append("=").append("Test for UPI").append("&").append("ti").append("=").append(Integer.parseInt("1000")).append("&").append("appid").append("=").append("1116").append("&").append("cu").append("=").append("INR").append("&").append("gcode").append("=").append("123466454").append("&").append("location").append("=").append("Mumbai,Maharashtra").append("&").append("ip").append("=").append("154fer53urn").append("&").append("os").append("=").append("android").append("&").append("payerAcAddressType").append("=").append("100").append("&").append("capability").append("=").append("453453d4f5343434df354").append("&").append("payeeName").append("=").append("890").append("&").append("payeeType").append("=").append("PERSON").append("&").append("PVA").append("=").append("100" + "@mapp").append("&").append("PayerVA").append("=").append("100");
        urlBuilder.append("upi://pay").append("?").append("REQUEST").append("=").append(payload2.toString()).append("&").append("TYPE").append("=").append("PAY");

        String deepLinkUrl = urlBuilder.toString();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(deepLinkUrl));
        //  intent.putExtra("data",deepLinkUrl);
        Intent chooser = Intent.createChooser(intent, "Pay With");
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            startActivityForResult(chooser, 1);
        }

    }

    public void sendRequesToPSPAppforMemberAccounttoSociety() {
        String DEEPLINKING_URL_BASE = "upi://pay";
        StringBuilder urlBuilder = new StringBuilder();
        int orderId = 100001 % new Random().nextInt();


        JSONObject payload2 = new JSONObject();
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            payload2.put("amount",editTextAmount.getText().toString().trim());
            payload2.put("payeeName", editTextMerchantName.getText().toString().trim());
            payload2.put("payeeAddress", editTextVPA.getText().toString().trim());

            payload2.put("payerAddress", sharedPreferences.getString("memberVPA", ""));
            payload2.put("payerName", sharedPreferences.getString("membername", ""));

            payload2.put("mobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));
        } catch (Exception e) {

        }

        //   urlBuilder.append("upi://pay").append("?").append("amount").append("=").append("100").append("&").append("ORDERID").append("=").append(Integer.parseInt("100")).append("&").append("credit_flag").append("=").append("1").append("&").append("appname").append("=").append("MGSAPP").append("&").append("am").append("=").append("100").append("&").append("tn").append("=").append(Integer.parseInt("100")).append("&").append("tr").append("=").append("Test for UPI").append("&").append("ti").append("=").append(Integer.parseInt("1000")).append("&").append("appid").append("=").append("1116").append("&").append("cu").append("=").append("INR").append("&").append("gcode").append("=").append("123466454").append("&").append("location").append("=").append("Mumbai,Maharashtra").append("&").append("ip").append("=").append("154fer53urn").append("&").append("os").append("=").append("android").append("&").append("payerAcAddressType").append("=").append("100").append("&").append("capability").append("=").append("453453d4f5343434df354").append("&").append("payeeName").append("=").append("890").append("&").append("payeeType").append("=").append("PERSON").append("&").append("PVA").append("=").append("100" + "@mapp").append("&").append("PayerVA").append("=").append("100");
        urlBuilder.append("upi://pay").append("?").append("REQUEST").append("=").append(payload2.toString()).append("&").append("TYPE").append("=").append("PAY");

        String deepLinkUrl = urlBuilder.toString();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(deepLinkUrl));
        //  intent.putExtra("data",deepLinkUrl);
        Intent chooser = Intent.createChooser(intent, "Pay With");
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            startActivityForResult(chooser, 1);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("Result Code:", String.valueOf(resultCode));
        try {
            if (requestCode == 1) {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String errorMsgStr = data.getStringExtra("RESPONSE");
                    if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(errorMsgStr);
                        String result= jsonObject.getString("result");
                        if(result.equalsIgnoreCase("SUCCESS")){
                            String resultDesc= jsonObject.getString("resultDesc");
                            Toast.makeText(context,resultDesc, Toast.LENGTH_LONG).show();
                           // sendNotificationConfirmationToAdmin();
                        }else{
                            String resultDesc= jsonObject.getString("resultDesc");
                            Toast.makeText(context, resultDesc, Toast.LENGTH_LONG).show();
                        }
                      dismiss();
                    }

                } else if (resultCode == Activity.RESULT_CANCELED) {

                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void sendCollectRequestMembertoAdmin() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        HashMap parameters = new HashMap();
        //  parameters.put("mobilenumber", "7529841084,9888762945");

        AppControler.getDataBaseInstance().addMemberApprovalPayRequest("","",editTextEventName.getText().toString().trim(),
                "",textDate.getText().toString().trim(),"","",editTextMerchantName.getText().toString().trim(),editTextVPA.getText().toString().trim(),
                editTextAmount.getText().toString().trim(),editTextDescription.getText().toString().trim(),
                textDate.getText().toString().trim(),"Pending",
                "100","200",sharedPreferences.getString("membername", ""),"","");


        parameters.put("aryOfNumbers", sharedPreferences.getString("adminMobile", ""));
        parameters.put("payerName", sharedPreferences.getString("name", ""));
        parameters.put("payerAddress", sharedPreferences.getString("communityVPA", ""));
        parameters.put("payeeName", editTextMerchantName.getText().toString().trim());
        parameters.put("payeeAddress", editTextVPA.getText().toString().trim());
        parameters.put("eventType", "");
        parameters.put("eventName", editTextEventName.getText().toString().trim());
        parameters.put("amount", editTextAmount.getText().toString().trim());
        parameters.put("startDate", textDate.getText().toString().trim());
        parameters.put("endDate", "");
        parameters.put("timeStamp", "");
        parameters.put("eventDesc", editTextDescription.getText().toString().trim());
        parameters.put("mobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));
        parameters.put("walletId", System.currentTimeMillis());
        parameters.put("ownName",sharedPreferences.getString("First name", ""));

        ParseCloud.callFunctionInBackground("push_notification_request_from_third", parameters, new FunctionCallback<Object>() {
            public void done(Object response, ParseException e) {
                if (e == null) {
                    Toast.makeText(context, "Notification has been sent successfully", Toast.LENGTH_LONG).show();
                    dismiss();
                } else {
                    Toast.makeText(context, "Failed, Please try again later", Toast.LENGTH_LONG).show();

                }

            }
        });

    }

    public void sendNotificationConfirmationToAdmin(){
        try{

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            HashMap parameters = new HashMap();

            parameters.put("aryOfNumbers", sharedPreferences.getString("adminMobile", ""));
            parameters.put("payerName", sharedPreferences.getString("name", ""));
            parameters.put("payerAddress", sharedPreferences.getString("communityVPA", ""));
            parameters.put("payeeName", editTextMerchantName.getText().toString().trim());
            parameters.put("payeeAddress", editTextVPA.getText().toString().trim());
            parameters.put("eventType", "");
            parameters.put("eventName", editTextEventName.getText().toString().trim());
            parameters.put("amount", editTextAmount.getText().toString().trim());
            parameters.put("startDate", textDate.getText().toString().trim());
            parameters.put("endDate", "");
            parameters.put("timeStamp", "");
            parameters.put("eventDesc", editTextDescription.getText().toString().trim());
            parameters.put("mobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));
            parameters.put("walletId", System.currentTimeMillis());
            parameters.put("ownName",sharedPreferences.getString("First name", ""));

            ParseCloud.callFunctionInBackground("push_notification_pay", parameters, new FunctionCallback<Object>() {
                public void done(Object response, ParseException e) {
                    if (e == null) {
                        Toast.makeText(context, "Notification has been sent successfully", Toast.LENGTH_LONG).show();
                        dismiss();
                    } else {
                        Toast.makeText(context, "Failed, Please try again later", Toast.LENGTH_LONG).show();

                    }

                }
            });


        }catch(Exception e){
            e.printStackTrace();
        }
    }

    protected void updatedate(int yr, int mon, int day) {
        try {



                Date toDate, fromDate = null;


                String dtStart = yr + "/" + (mon + 1) + "/" + day;
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                try {
                    fromDate = format.parse(dtStart);

                    dtStart = format.format(fromDate);

                } catch (Exception e) {
                    // TODO Auto-generated catch block

                }




            textDate.setText(dtStart);








        } catch (Exception e) {

        }
    }

    public void sendPayRequestforMemberApproval(){
        try{


            String url=Constants.CORE_URL+Constants.PAYMENTREQUEST_SUFFIX;

            HashMap<String, String> list = new HashMap<String, String>();

            list.put("groupId", sharedPreferences.getString("groupId", ""));
            list.put("userid", sharedPreferences.getString("userid",""));

            list.put("msisdn", sharedPreferences.getString("adminMobile",""));
            list.put("eventType", "");
            list.put("eventName", editTextEventName.getText().toString().trim());
            list.put("ammount", editTextAmount.getText().toString().trim());

            list.put("payerName", "");
            list.put("payerAddress", "");
            list.put("payeeName", sharedPreferences.getString("name", ""));
            list.put("payeeAddress", sharedPreferences.getString("communityVPA", ""));

            list.put("startDate", textDate.getText().toString());

            list.put("eventDesc", editTextDescription.getText().toString().trim());
            list.put("mobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));


            list.put("name", sharedPreferences.getString("name", ""));

            list.put("endDate", "");
            list.put("timeStamp", "");


            list.put(Constants.IMEI,
                    CommonUtility.getImeiNumber(context));
            list.put(Constants.DEVICE_ID,
                    CommonUtility.getDeviceId(context));
            list.put(Constants.EMAIL_ID,
                    CommonUtility.getEmailId(context));
            list.put(Constants.MOBILE_OS, "Android"
                    + Build.VERSION.RELEASE);
            list.put("mobileApp", "true");
            list.put("mobileAppVersion", CommonUtility.getVersionCode(context));


            serverCommunication(url,list,Constants.PAYMENTREQUEST);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void serverCommunication(String URL, HashMap<String, String> list,String resultCode) {


        if (CheckInternetConnection.haveNetworkConnection(context)) {
            try {
                requestClass = new CustomDialogNetworkRequest(FragmentAddNewPayment.this, context);
                if (requestClass == null) {
                    requestClass = new CustomDialogNetworkRequest(FragmentAddNewPayment.this, context);

                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                } else {
                    requestClass.makePostRequest(URL, true, list, resultCode,"");
                }
            } catch (Exception e) {

            }


        } else {
            CommonUtility.showNointernetDialog(context);
        }
    }

    @Override
    public void onResult(String result, String responseCode) {
        if (responseCode.equalsIgnoreCase(Constants.PAYMENTREQUEST)) {
            try{
                if(!result.equalsIgnoreCase("") && result != null) {

                    JSONObject responsejsonData = new JSONObject(result);
                    String response = responsejsonData.optString("response");

                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.optString("status");
                    if(status.equalsIgnoreCase("S")){
                        AppControler.getDataBaseInstance().addMemberApprovalPayRequest("","",editTextEventName.getText().toString().trim(),
                                "",textDate.getText().toString().trim(),"","",editTextMerchantName.getText().toString().trim(),editTextVPA.getText().toString().trim(),
                                editTextAmount.getText().toString().trim(),editTextDescription.getText().toString().trim(),
                                textDate.getText().toString().trim(),"Pending",
                                "100","200",sharedPreferences.getString("membername", ""),"","");


                    }else{
                        String errorCode = jsonData.optString("errorCode");


                    }

                }else{

                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }


}
