package spicedigital.in.npcimerchantapp.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.EventAddMemberAdapter;
import spicedigital.in.npcimerchantapp.support_classes.Contact;

/**
 * Created by CH-E01073 on 19-03-2016.
 */

/**
 * Created by ch-e00812 on 3/19/2016.
 */
public class EventAddMembers extends DialogFragment {


    Context context;
    View rootView;

    RecyclerView recyclerView;

    Button btnCancel, btnAdd;

    LinearLayout progressBarLay;

    public static Handler checkBoxHandlers;

    //    String checkBoxCount;
    ArrayList<String> checkedPosList;

    public EventAddMembers() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_member, container,
                false);
        context = getActivity();
        initilizeView(rootView);

        checkedPosList = new ArrayList<>();

        new FetchContactTask().execute();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return rootView;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        try {
            dialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
        }
        return dialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            // safety check
            if (getDialog() == null) {
                return;
            }

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;

            getDialog().getWindow().setLayout((int) (width * .98f), ViewGroup.LayoutParams.WRAP_CONTENT);


        } catch (Exception e) {
        }
    }


    private void initilizeView(View mrootView) {

        recyclerView = (RecyclerView) mrootView.findViewById(R.id.recyclerView);
        btnCancel = (Button) mrootView.findViewById(R.id.btnCancel);
        btnAdd = (Button) mrootView.findViewById(R.id.btnAdd);
        progressBarLay = (LinearLayout) mrootView.findViewById(R.id.progressBarLay);
    }



    private class FetchContactTask extends AsyncTask<Void, Void, ArrayList<Contact>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBarLay.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

            checkBoxHandlers = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);

                    if (msg.what == 1000) {
                        checkedPosList = (ArrayList<String>) msg.obj;
                        btnAdd.setText("ADD " + checkedPosList.size() + " MEMBERS");
                    }

                }
            };

        }

        @Override
        protected ArrayList<Contact> doInBackground(Void... params) {
            try {
                ArrayList<Contact> contactArrayList = new ArrayList<Contact>();
                contactArrayList = FragmentAddNewCollectionRequest.getContactArray();
                return contactArrayList;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(final ArrayList<Contact> result) {
            super.onPostExecute(result);

            progressBarLay.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            if (result != null) {
                EventAddMemberAdapter addMembersAdapter = new EventAddMemberAdapter(context, result);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(addMembersAdapter);

                for(int i=0; i<result.size(); i++)
                {
                    Log.d("checked", ""+result.get(i).isChecked());
                }





                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        for (int i = 0; i < checkedPosList.size(); i++) {

                            String selectedNumber = result.get(Integer.parseInt(checkedPosList.get(i))).getNumber();
                            selectedNumber = selectedNumber.replace("-", "");
                            selectedNumber = selectedNumber.replace(" ", "");

                            if (selectedNumber.length() == 0) {
                                // Toast.makeText(getActivity(), "", 700).show();
                                AlertDialog.Builder alert2 = new AlertDialog.Builder(
                                        getActivity());
                                alert2.setMessage("No Mobile number found");
                                alert2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {

                                            }
                                        });
                                alert2.setCancelable(true);
                                alert2.show();

                                // showDialog("No Mobile number found");
                            } else if (selectedNumber.length() > 10) {
                                selectedNumber = selectedNumber
                                        .substring(selectedNumber.length() - 10);
                            } else {
                                //saveDataToParse(result.get(Integer.parseInt(checkedPosList.get(i))).getName(), selectedNumber);

                                FragmentAddNewCollectionRequest.setAddMembers(selectedNumber);
                            }
/*
                            Log.d("name------------", "" + result.get(Integer.parseInt(checkedPosList.get(i))).getName());
                            Log.d("number------------", "" + result.get(Integer.parseInt(checkedPosList.get(i))).getNumber());*/
                        }

                        dismiss();
                        //startActivity(new Intent(context, MainActivity.class));

                    }
                });


            }


        }

        private void saveDataToParse(final String memberName, final String mobileNumber) {
        /*ParseQuery<ParseObject> queryUserAccount = new ParseQuery<ParseObject>(
                "Admin");
        queryUserAccount.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null) {*/
            ParseQuery<ParseObject> queryUserAccount = new ParseQuery<ParseObject>(
                    "Member");
            queryUserAccount.whereEqualTo("mobileNumber",
                    mobileNumber);
            queryUserAccount.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if (e == null) {


                    } else {
                        ParseObject member = new ParseObject("Member");

                        if (mobileNumber != null && mobileNumber.length() > 0) {
                            member.put("mobileNumber", mobileNumber);
                            member.put("memberName", memberName);
                            member.saveInBackground();
                        }
                    }
                }
            });


            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("memberName", memberName);
            editor.commit();

            // String mobileNumber = sharedPreferences.getString(Constants.mobileNumber, "");

           /* parseObject.put("communityName", editTextCommunityName.getText().toString());
            parseObject.put("name", edittextName.getText().toString());
            parseObject.put("communityType", editTextCommunityType.getText().toString());
            parseObject.put("communityDesc", editTextDesc.getText().toString());
            parseObject.put("communityVPA", editTextVPA.getText().toString());
            parseObject.put("communityId", editTextCommunityName.getText().toString() + new Random().nextInt());*/

              /*  }
            }
        });*/
        }
    }


}
