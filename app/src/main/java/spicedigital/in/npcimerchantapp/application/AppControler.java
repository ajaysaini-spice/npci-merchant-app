package spicedigital.in.npcimerchantapp.application;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.database.MyDataBase;
import spicedigital.in.npcimerchantapp.support_classes.Constants;

/**
 * Created by CH-E01006 on 3/18/2016.
 */
public class AppControler extends Application {

    private static MyDataBase mDtatabaseInstance = null;
    private static AppControler sInstance;
    private String installationId;

    public static MyDataBase getDataBaseInstance() {
        /**
         * using the application context as suggested by CommonsWare.
         * this will ensure that you dont accidentally leak an Activitys
         * context
         */

        try {
            if (mDtatabaseInstance == null) {
                mDtatabaseInstance = new MyDataBase(sInstance.getApplicationContext());
            }
        } catch (Exception e) {

        }
        return mDtatabaseInstance;
    }

    /**
     * A helper method to get the application context.
     *
     * @return {@link android.content.Context}
     */
    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }
    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;

        try {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... voids) {
                    getDataBaseInstance();
                    return null;
                }
            }.execute();

        } catch (Exception e) {

        }

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        // Add your initialization code here
        Parse.initialize(this, getString(R.string.parse_app_id),
                getString(R.string.parse_client_key));

        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    String deviceToken = (String) ParseInstallation.getCurrentInstallation().get("deviceToken");
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("deviceToken", deviceToken).commit();
                }
            }
        });

        ParseInstallation.getCurrentInstallation().saveInBackground();
        installationId = ParseInstallation.getCurrentInstallation()
                .getInstallationId();


        if (installationId == null) {
            installationId = "";
        }

        PreferenceManager.getDefaultSharedPreferences(getAppContext()).edit().putString(Constants.PARSE_INSTALLATION_ID, installationId).commit();
    }
}
